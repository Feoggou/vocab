from setuptools import setup, find_packages


setup(
    name='vocabulary',
    version='0.5.1',
    packages=find_packages(),
    description='Vocabulary for the CLI',
    author='Samuel Ghinet',
    author_email='fio_244@yahoo.com',
    entry_points={
        'console_scripts': [
            'vocabulary = vocabulary.vocab:run_app'
        ]
    },
    setup_requires=[
        'setuptools',
    ],
    python_requires='>=3.6',
)
