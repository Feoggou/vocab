import itertools
import shlex
from typing import Optional

from vocabulary.utils.errors import *


def find_index_if(collection, condition):
    for i, x in enumerate(collection):
        if condition(x):
            return i
    return None


def all_nonempty(collection):
    T = type(collection)
    return T(x for x in collection if x)


def bind_not(func: callable)-> callable:
    return lambda *args, **kwargs: not func(*args, **kwargs)


class CommandParser:
    def parse(self, line: str) -> Optional[dict]:
        if not line:
            return None

        lexical_items = shlex.split(line)
        command_and_options, target_items = self._strip_target_items(lexical_items)

        command_items = list(itertools.takewhile(self._item_is_command, command_and_options))
        command_options = list(itertools.dropwhile(bind_not(self._item_is_option), command_and_options))

        command = {
            "name": self._build_command_name(command_items),
            "options": self._build_command_options(command_options),
            "target": self._build_target(target_items)
        }

        self._verify_command_has_name(command["name"])
        self._verify_options_without_value(command["options"])
        self._verify_options_without_name(command["options"])

        return command

    @property
    def target_name(self) -> str:
        return ""

    @classmethod
    def _item_is_command(cls, item: str):
        return not cls._item_is_option(item)

    @classmethod
    def _item_is_option(cls, item: str):
        return "=" in item

    @staticmethod
    def _build_command_name(command_items):
        command_items = list(filter(None, command_items))
        return " ".join(command_items)

    @staticmethod
    def _build_command_options(option_items):
        return {name: value for name, value in [option.split("=") for option in option_items]}

    @staticmethod
    def _build_target(target_items):
        return " ".join(target_items) if target_items else ""

    @staticmethod
    def _strip_target_items(lexical_items: list) -> tuple:
        index = find_index_if(lexical_items, lambda item: item.endswith(':'))
        if index is None:
            return lexical_items, None
        # remove ending ':' from the command / option item -- it is simply a marker
        lexical_items[index] = lexical_items[index].rstrip(':')
        return all_nonempty(lexical_items[:index+1]), lexical_items[index+1:]

    @staticmethod
    def _verify_options_without_value(options: dict):
        options_without_value = [name for name in options.keys() if not options[name]]
        if len(options_without_value):
            raise BadlyFormattedCommand("Found one or more options without a value set: " +
                                        ",".join(options_without_value))

    @staticmethod
    def _verify_options_without_name(options: dict):
        options_without_name = [options[name] for name in options.keys() if not name]
        if len(options_without_name):
            raise BadlyFormattedCommand("Found one or more values without a name for the option specified: " +
                                        ",".join(options_without_name))

    @staticmethod
    def _verify_command_has_name(name: str):
        if not name:
            raise BadlyFormattedCommand("Command name missing.")

