from vocabulary.app.word.word_repository import WordRepository
from vocabulary.cli.parser.cmd_finder import CommandFinder
from vocabulary.cli.commands import Command


from vocabulary.cli.parser.cmd_parser import CommandParser


class CommandBuilder:
    """ Class Kind: UseCase / Entity
        Critical Business Rule: Can't have a CLI for this application without a Command Parser
        But... it feels more like Application Specific, rather than Business specific.

        Responsibility: Construct a Command object, based on the input text.

    """
    def __init__(self, repo: WordRepository, finder: CommandFinder, parser: CommandParser):
        self._repo = repo
        self._command_finder = finder
        self._command_parser = parser

    def build_from_line(self, text_line: str) -> Command:
        command = self._command_parser.parse(text_line)

        command_class = self._command_finder.find_class(command["name"])
        return command_class(repo=self._repo, target=command["target"], opts=command["options"])
