from vocabulary.cli.commands import *
from vocabulary.utils.errors import CommandNotFoundError


class CommandFinder:
    COMMAND_MAP = {
        "add": NewBareCommand,
        "add def": AddDefinitionCommand,
        "add idiom": AddIdiomCommand,
        "add domain": AddDomainCommand,
        "add style": AddStyleCommand,
        "add object": AddObjectCommand,
        "add parent": AddParentCommand,
        "add subject": AddSubjectCommand,
        "search": SearchExplanationCommand,
        "usage": UsageCommand,
        "show domains": ShowDomainsCommand,
        "show styles": ShowStylesCommand,
        "show parents": ShowParentCommand,
        "show subjects": ShowSubjectsCommand,
        "show objects": ShowObjectsCommand,
        "rename parent": RenameParentCommand,
        "display": DisplayCommand,
        "print": DisplayCommand,
        "p": DisplayCommand,
        "help": HelpCommand,
        "list": ListCommand,
        "quit": QuitCommand,
        "exit": QuitCommand,
        "vim": VimCommand,
    }

    def find_class(self, command_name: str) -> type(Command):
        if command_name not in self.COMMAND_MAP.keys():
            raise CommandNotFoundError(command_name)

        return self.COMMAND_MAP[command_name]
