from vocabulary.cli.commands.command import Command

from vocabulary.app.search.filter.gram_value import GramValueFilter
from vocabulary.app.search.filter.domain import DomainFilter
from vocabulary.app.search.filter.subject import SubjectFilter
from vocabulary.app.search.filter.object import ObjectFilter
from vocabulary.app.search.filter.parent import ParentFilter
from vocabulary.app.search.filter.usefulness import UsefulnessFilter
from vocabulary.app.search.filter.frequency import FrequencyFilter
from vocabulary.app.search.filter.style import StyleFilter

# from ..search.filter import *
from vocabulary.app.search.method_strategy import SearchMethodStrategy
from vocabulary.app.search.where import SearchIn
from vocabulary.app.search.search import Search


# TODO: refactor
class SearchCommand(Command):
    def __init__(self, target: str, opts: dict, search_in: SearchIn):
        Command.__init__(self, target, opts)

        search_method = SearchMethodStrategy(search_in).pick_method_for(target)

        self._filters = self.create_filters(opts)
        self._searcher = Search(target, self._filters, search_method)

    def get_filters(self):
        return self._filters

    @staticmethod
    def create_filters(options: dict):
        # TODO: needs unit test.
        # TODO: is THIS the best place to hold the "create_filters" function?
        filters = []

        if options is None or len(options) == 0:
            return filters

        for key in options.keys():
            value = options[key]

            all_filters = {
                "gram": GramValueFilter(value),
                "domain": DomainFilter(value),
                "subject": SubjectFilter(value),
                "object": ObjectFilter(value),
                "parent": ParentFilter(value),
                "usefulness": UsefulnessFilter(value),
                "freq": FrequencyFilter(value),
                "frequency": FrequencyFilter(value),
                "style": StyleFilter(value),
            }

            if key not in all_filters.keys():
                # TODO: need specialized Exception class and proper test
                message = "Bad option. Available are: " + ", ".join(all_filters.keys())
                raise RuntimeError(message)

            # TODO: if wrong key?
            filters.append(all_filters[key])

        return filters

    def execute(self):
        return self._searcher.execute()
