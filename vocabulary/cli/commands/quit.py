from .command import Command

from vocabulary.utils.errors import QuitException


# TODO: refactor
class QuitCommand(Command):
    def execute(self):
        raise QuitException()
