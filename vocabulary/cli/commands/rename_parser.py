from vocabulary.utils.errors import *


# TODO: refactor
class RenameCommandParser:
    @staticmethod
    def _validate(expression: str):
        if "->" not in expression:
            raise BadlyFormattedCommand("No '->' found!")

        if expression.count("->") != 1:
            raise BadlyFormattedCommand("Expected only one '->'")

    @staticmethod
    def _trim_argument(arg: str):
        arg = arg.strip(' ')
        arg = arg.strip('"')
        return arg

    @staticmethod
    def parse_expression(expression: str):
        RenameCommandParser._validate(expression)

        items = expression.split("->")

        old = RenameCommandParser._trim_argument(items[0])
        new = RenameCommandParser._trim_argument(items[1])

        return old, new
