from .command import Command

from vocabulary.app.tag.add_domain_tag import AddDomainTag


# TODO: refactor
class AddDomainCommand(Command):
    def __init__(self, target: str, opts: dict=None):
        Command.__init__(self, target, opts)

    def execute(self):
        adder = AddDomainTag(tag=self.target)

        adder.do()

        return None
