from .command import Command
from vocabulary.domain.word import Word


# TODO: refactor
class DisplayCommand(Command):
    def __init__(self, target: str, opts: dict=None):
        Command.__init__(self, target, opts)

    def execute(self):
        # TODO: I want to load the "root word" from the file also -- and check it against file name?
        word = Word.load(self.target)

        return word
