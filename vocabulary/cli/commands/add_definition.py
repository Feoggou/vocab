from .command import Command

from vocabulary.app.word.word_repository import WordRepository
from vocabulary.app.word.updater import DefinitionUpdater
from vocabulary.app.operations.create import DefinitionCreator

from vocabulary.utils.utilities import ConsolePrinter


class AddDefinitionCommand(Command):
    def __init__(self, repo: WordRepository, target: str, opts: dict = None):
        Command.__init__(self, repo, target, opts)
        self._impl = None

    def execute(self):
        if self._impl is None:
            self._initialize(self.target)

        self._execute()
        ConsolePrinter.output_message("\nAdded word: {}\n\n".format(self.target))

    def _initialize(self, target):
        self._impl = DefinitionUpdater(target) if self.repository.have_word(target) else DefinitionCreator(target)

    def _execute(self):
        self._impl.execute()
