from .command import Command

from vocabulary.app.tag.add_subject_tag import AddSubjectTag


# TODO: refactor
class AddSubjectCommand(Command):
    def __init__(self, target: str, opts: dict=None):
        Command.__init__(self, target, opts)

    def execute(self):
        adder = AddSubjectTag(tag=self.target)

        adder.do()

        return None
