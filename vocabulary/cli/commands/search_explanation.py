
from ..commands.search import SearchCommand

from vocabulary.app.search.where import SearchInExplanations


# TODO: refactor
class SearchExplanationCommand(SearchCommand):
    def __init__(self, target: str="", opts: dict=None):
        SearchCommand.__init__(self, target, opts, SearchInExplanations())
