from .command import Command

from vocabulary.app.tag.show_parents import ShowParent


# TODO: refactor
class ShowParentCommand(Command):
    def execute(self):
        show = ShowParent()
        return show.do()
