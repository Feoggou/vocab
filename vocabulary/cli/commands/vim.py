import os
import subprocess

from vocabulary.utils import config
from vocabulary.utils.errors import *

from .command import Command


# TODO: refactor
class VimCommand(Command):
    def __init__(self, target: str, opts: dict=None):
        Command.__init__(self, target, opts)

    def execute(self):
        file_name = self.target + ".word"
        full_file_name = os.path.join(config.OUTPUT_JSON_DIR, file_name)

        if not os.path.exists(full_file_name):
            raise WordNotFoundError(self.target)

        subprocess.run(["vim", full_file_name])

        return None
