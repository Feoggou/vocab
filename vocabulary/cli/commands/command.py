import abc

from vocabulary.app.word.word_repository import WordRepository


# TODO: refactor
class Command(metaclass=abc.ABCMeta):
    def __init__(self, repo: WordRepository, target: str="", opts: dict=None):
        self._repo = repo
        self._target = target
        self._options = opts

    @property
    def repository(self):
        return self._repo

    @property
    def options(self):
        return self._options

    @property
    def target(self):
        return self._target

    @abc.abstractmethod
    def execute(self):
        pass
