from .command import Command

COMMAND_HELP = {
            "new def": "Creates a new dictionary file, named by the root word, where it writes one definition.\n"
                       "\t\t\tNOTE: It assumes the file does not already exist.",
            "new idiom": "Creates a new dictionary file, named by the root word, where it writes one idiom.\n"
                         "\t\t\tNOTE: It assumes the file does not already exist.",
            "add def": "Adds one definition to an existing file, named by the root word",
            "add idiom": "Adds one idiom to an existing file, named by the root word",
            "add domain": "Appends a Domain word in the domain file. \n"
                         "\t\t\tDomain = e.g. architecture, medicine, etc.",
            "add parent": "Appends a Parent word in the parents file = e.g. Synonym",
            "add subject": "Appends a Subject word in the subjects file",
            "show domains": "Show what Domains we have available\n"
                          "\t\t\tDomain = e.g. architecture, medicine, etc.",
            "show parents": "Show what Parents we have available = e.g. Synonym",
            "show subjects": "Show what Subjects we have available",
            "display": "Displays contents of a definitions file, as colored text",
            "list": "Lists all available words, or only words matching regex",
            "help": "Shows this help"
        }


# TODO: refactor
class HelpCommand(Command):
    def execute(self):
        text_items = ["{:20}{}".format(cmd, COMMAND_HELP[cmd]) for cmd in COMMAND_HELP.keys()]

        print("\n".join(text_items) + "\n")

        return None
