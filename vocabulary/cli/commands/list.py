import os
import re

from vocabulary.utils import config

from .command import Command


# TODO: refactor
class ListCommand(Command):
    def __init__(self, target: str="", opts: dict=None):
        Command.__init__(self, target, opts)

    def execute(self):
        all_words = [x.replace(".word", "") for x in os.listdir(config.OUTPUT_JSON_DIR) if x.endswith(".word")]

        filtered_words = all_words

        if len(self.target):
            expression = self.target
            assert expression == expression.strip()

            pattern = re.compile(expression)
            filtered_words = [word for word in all_words if re.match(pattern, word)]

        filtered_words = sorted(filtered_words)
        text = "\n".join(filtered_words)
        print(text)

        return None
