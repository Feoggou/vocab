from .command import Command

from vocabulary.app.tag.show_domains import ShowDomains


# TODO: refactor
class ShowDomainsCommand(Command):
    def execute(self):
        show = ShowDomains()
        return show.do()
