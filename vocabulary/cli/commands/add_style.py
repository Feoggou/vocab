from .command import Command

from vocabulary.app.tag.add_style_tag import AddStyleTag


# TODO: refactor
class AddStyleCommand(Command):
    def __init__(self, target: str, opts: dict=None):
        Command.__init__(self, target, opts)

    def execute(self):
        adder = AddStyleTag(tag=self.target)

        adder.do()

        return None
