from .command import Command

from vocabulary.app.tag.add_object_tag import AddObjectTag


# TODO: refactor
class AddObjectCommand(Command):
    def __init__(self, target: str, opts: dict=None):
        Command.__init__(self, target, opts)

    def execute(self):
        adder = AddObjectTag(tag=self.target)

        adder.do()

        return None
