from vocabulary.app.word.word_repository import WordRepository
from .command import Command

from vocabulary.app.word.updater import IdiomUpdater
from vocabulary.app.operations.create import IdiomCreator

from vocabulary.utils import utilities


# TODO: refactor
class AddIdiomCommand(Command):
    def __init__(self, repo: WordRepository, target: str, opts: dict=None):
        Command.__init__(self, repo, target, opts)
        self._impl = None

    def _initialize(self, target):
        all_words = utilities.get_vocab_words()
        if self.target in all_words:
            self._impl = IdiomUpdater(target)
        else:
            self._impl = IdiomCreator(target)

    def _execute(self):
        self._impl.execute()

    def execute(self):
        if self._impl is None:
            self._initialize(self.target)

        self._execute()
