from .command import Command

from vocabulary.app.operations.create import BareWordCreator


# TODO: refactor
class NewBareCommand(Command):
    def __init__(self, target: str, opts: dict=None):
        Command.__init__(self, target, opts)
        self._creator = BareWordCreator(target)

    def execute(self):
        self._creator.execute()
