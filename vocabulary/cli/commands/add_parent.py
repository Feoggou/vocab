from .command import Command

from vocabulary.app.tag.add_parent_tag import AddParentTag


# TODO: refactor
class AddParentCommand(Command):
    def __init__(self, target: str, opts: dict=None):
        Command.__init__(self, target, opts)

    def execute(self):
        adder = AddParentTag(tag=self.target)

        adder.do()

        return None
