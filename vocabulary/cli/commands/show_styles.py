from .command import Command

from vocabulary.app.tag.show_styles import ShowStyles


# TODO: refactor
class ShowStylesCommand(Command):
    def execute(self):
        show = ShowStyles()
        return show.do()
