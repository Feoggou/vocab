from .command import Command

from vocabulary.app.tag.show_objects import ShowObjects


# TODO: refactor
class ShowObjectsCommand(Command):
    def execute(self):
        show = ShowObjects()
        return show.do()
