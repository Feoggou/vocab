from .command import Command
from .rename_parser import RenameCommandParser

from vocabulary.app.tag.parent_catalog import ParentCatalog
from vocabulary.app.word.word_repository import WordRepository


# TODO: refactor
class RenameParentCommand(Command):
    def __init__(self, repo: WordRepository, target: str, opts: dict=None):
        Command.__init__(self, repo, target, opts)

    def execute(self):
        parser = RenameCommandParser()

        old, new = parser.parse_expression(self.target)

        catalog = ParentCatalog()
        catalog.rename_item(old, new)

        self.repository.rename_tag("parents", old, new)
