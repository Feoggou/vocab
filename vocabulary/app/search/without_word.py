from .method import SearchMethod, SearchIn


# TODO: refactor & test
class SearchWithoutWord(SearchMethod):
    def __init__(self, search_in: SearchIn):
        SearchMethod.__init__(self, "", search_in)

    def match(self, text: str):
        return text

    def find_matches_in_entries(self, entries):
        matches = self._search_in.search(self.match, entries)
        return matches

    def find_matches_in_idiom_expression(self, entries):
        matches = self._search_in.search_in_idiom_expression(self.match, entries)
        return matches
