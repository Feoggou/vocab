

# TODO: refactor & test
class FoundInWord:
    def __init__(self, word_name: str, matching_definitions: list):
        self._word_name = word_name
        self._matching_definitions = matching_definitions

    def __eq__(self, other):
        return self._word_name == other._word_name and \
               set(self._matching_definitions) == set(other._matching_definitions)


# TODO: refactor & test
class SearchResult:
    def __init__(self, result_items: list):
        self._result_items = result_items

    def get_items(self):
        return self._result_items
