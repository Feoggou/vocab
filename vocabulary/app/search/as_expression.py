import re

from .method import SearchMethod, SearchIn


# TODO: refactor & test
class SearchAsExpression(SearchMethod):
    def __init__(self, expression: str, search_in: SearchIn):
        SearchMethod.__init__(self, expression, search_in)

    def match(self, text: str):
        pattern = re.compile(r'\b' + self._what + r'\b', flags=re.IGNORECASE)
        match = pattern.search(text)
        if match is None:
            return None

        match_item = re.sub(pattern, SearchMethod._colored(self._what), text)
        return match_item

    def find_matches_in_entries(self, entries):
        matches = self._search_in.search(self.match, entries)
        return matches

    def find_matches_in_idiom_expression(self, entries):
        matches = self._search_in.search_in_idiom_expression(self.match, entries)
        return matches
