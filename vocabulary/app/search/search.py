from vocabulary.app.search.result import SearchResult, FoundInWord
from vocabulary.app.search.method import NoSearchMethod, SearchMethod

from vocabulary.domain.word import Word
from vocabulary.app.word.matcher import DefinitionContentMatcher

from vocabulary.utils import utilities


# TODO: refactor & test
class Search:
    def __init__(self, word: str, filters: list, search_method: SearchMethod=NoSearchMethod):
        # TODO: word or expression
        self._word = word
        self._filters = filters
        self._search_method = search_method

    def _apply_filters(self, word: Word):
        for f in self._filters:
            word = f.do(word)

        return word

    def _find_matches(self, in_word: Word):
        matcher = DefinitionContentMatcher(in_word, self._search_method)

        matches = matcher.find_matches()
        if len(matches) == 0:
            return None
        return matches

    def _search_in_word(self, found_word: Word):
        found_word = self._apply_filters(found_word)

        matches = self._find_matches(found_word)
        # TODO: not unit test for this!
        if matches is None:
            return None

        new_item = FoundInWord(found_word.word_name, matches)
        return new_item

    def search_files(self) -> SearchResult:
        results = []

        for f in utilities.get_vocab_words():
            found_word = Word.load(f)

            new_item = self._search_in_word(found_word)
            if new_item is not None:
                results.append(new_item)

        return SearchResult(results)

    def execute(self):
        search_result = self.search_files()

        return search_result
