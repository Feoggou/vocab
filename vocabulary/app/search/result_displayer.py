from .result import SearchResult, FoundInWord

from vocabulary.utils import config


# TODO: refactor & test
class SearchResultDisplayer:
    def __init__(self, search_result: SearchResult):
        self._result = search_result

    @staticmethod
    def _output_results_in_word(item: FoundInWord):
        print(config.COLOR_GREEN + item._word_name + config.COLOR_RESET + ":")

        for explanation in item._matching_definitions:
            print(explanation)

    def execute(self):
        for item in self._result.get_items():
            self._output_results_in_word(item)

        print("")
