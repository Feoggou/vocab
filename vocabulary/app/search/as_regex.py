import re

from .method import SearchMethod, SearchIn


# TODO: refactor & test
class SearchAsRegex(SearchMethod):
    def __init__(self, expression: str, search_in: SearchIn):
        SearchMethod.__init__(self, expression, search_in)
        self._pattern = re.compile('(' + self._what + ')', flags=re.IGNORECASE)

    def match(self, text: str):
        match = self._pattern.search(text)
        if match is None:
            return None

        match_item = re.sub(self._pattern, self._colored(r'\1'), text)
        return match_item

    def find_matches_in_entries(self, entries):
        matches = self._search_in.search(self.match, entries)
        return matches

    def find_matches_in_idiom_expression(self, entries):
        matches = self._search_in.search_in_idiom_expression(self.match, entries)
        return matches
