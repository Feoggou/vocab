from vocabulary.utils import config

from .where import SearchIn


# TODO: refactor & test
class SearchMethod:
    def __init__(self, what, search_in: SearchIn):
        self._what = what
        self._search_in = search_in

    def match(self, text: str):
        raise NotImplementedError()

    @property
    def what(self):
        return self._what

    # TODO: move to utilities
    @staticmethod
    def _colored(text: str):
        return config.COLOR_BOLDRED + text + config.COLOR_RESET

    def find_matches_in_entries(self, entries):
        raise NotImplementedError()

    def find_matches_in_idiom_expression(self, entries):
        raise NotImplementedError()


# TODO: refactor & test
class NoSearchMethod(SearchMethod):
    def find_matches_in_entries(self, entries):
        return None

    def find_matches_in_idiom_expression(self, entries):
        return None

    def match(self, text: str):
        return None
