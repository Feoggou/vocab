import re

from .as_expression import SearchAsExpression
from .as_regex import SearchAsRegex
from .where import SearchIn
from .without_word import SearchWithoutWord


# TODO: refactor & test
class SearchMethodStrategy:
    def __init__(self, search_in: SearchIn):
        self._search_in = search_in

    def _pick_regex_or(self, word_or_expr: str, DefaultMethod):
        if re.match(r'^[a-zA-Z0-9 \-]+$', word_or_expr):
            return DefaultMethod(word_or_expr, self._search_in)

        return SearchAsRegex(word_or_expr, self._search_in)

    def pick_method_for(self, word_or_expr: str):
        assert not word_or_expr.startswith(" ")
        assert not word_or_expr.endswith(" ")

        if len(word_or_expr) == 0:
            return SearchWithoutWord(self._search_in)

        if word_or_expr.startswith('"') and word_or_expr.endswith('"'):
            return self._pick_regex_or(word_or_expr[1:-1], SearchAsExpression)

        return self._pick_regex_or(word_or_expr, SearchAsExpression)
