

# TODO: refactor & test
class SearchIn:
    def search(self, match_func, entries) -> list:
        raise NotImplementedError()

    @staticmethod
    def search_in_idiom_expression(match_func, idioms):
        matches = [match_func(idiom.expression) for idiom in idioms]
        matches = [x for x in matches if x is not None]

        return matches


# TODO: refactor & test
class SearchNowhere(SearchIn):
    def search(self, match_func, entries) -> list:
        return []


# TODO: refactor & unit test - same as SearchInExamples
class SearchInExplanations(SearchIn):
    def search(self, match_func, entries) -> list:
        matches = []

        for entry in entries:
            new_matches = [match_func(explanation) for explanation in entry.explanations]
            new_matches = [x for x in new_matches if x is not None]
            matches += new_matches

        return matches


# TODO: refactor & test
class SearchInExamples(SearchIn):
    def search(self, match_func, entries) -> list:
        matches = []

        for entry in entries:
            new_matches = [match_func(example) for example in entry.examples]
            new_matches = [x for x in new_matches if x is not None]
            matches += new_matches

        return matches
