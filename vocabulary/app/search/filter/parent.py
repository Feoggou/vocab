from vocabulary.app.search.filter.definition import DefinitionFilter

from vocabulary.app.tag.parent_catalog import ParentHierarchy

from vocabulary.domain.entries.definition import Definition


# TODO: refactor & test
class ParentFilter(DefinitionFilter):
    def __init__(self, value: str):
        DefinitionFilter.__init__(self, value, self.condition)

    def condition(self, definition: Definition) -> bool:
        for parent in definition.parents:
            if ParentHierarchy(parent).have_sequence(self._value):
                return True

        return False
