from vocabulary.app.search.filter.definition import DefinitionFilter

from vocabulary.domain.entries.definition import Definition


# TODO: refactor & test
class UsefulnessFilter(DefinitionFilter):
    def __init__(self, value: str):
        DefinitionFilter.__init__(self, value, self._matches)

    def _matches(self, definition: Definition):
        if self._value == "-1" or self._value == "0":
            return definition.usefulness == self._value

        # TODO: what if not convertible to int?
        return int(definition.usefulness) >= int(self._value)
