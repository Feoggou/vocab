from vocabulary.domain.entries.definition import Definition
from vocabulary.domain.entries.derived_forms import DerivedForms


# TODO: refactor & test
class DefinitionsMatcher:
    def __init__(self, definitions: list):
        self._definitions = definitions

    @staticmethod
    def _matches(gram_value: str, definition: Definition) -> bool:
        # If searching by "vb" (i.e. vt + vi), then find both vt-s and vi-s
        if gram_value == "vb":
            return definition.gram_value == "vb" or definition.gram_value == "vt" or definition.gram_value == "vi"

        # If searching by "vt" or by "vi", find also the generic "vb" (i.e. vt + vi)
        if definition.gram_value == "vb":
            return gram_value == "vb" or gram_value == "vt" or gram_value == "vi"

        return definition.gram_value == gram_value

    # TODO: no unit test
    @staticmethod
    def _have(gram_value: str, forms: DerivedForms):
        if gram_value == "vt" or gram_value == "vi":
            return forms.have("verb")

        return forms.have(gram_value)

    def find_matches(self, gram_value: str, forms: DerivedForms=None):
        if self._definitions is None:
            return None

        new_definitions = [definition for definition in self._definitions if self._matches(gram_value, definition)]

        if len(new_definitions):
            return new_definitions
        else:
            if forms is not None and self._have(gram_value, forms):
                return self._definitions
            return []
