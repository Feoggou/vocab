from vocabulary.domain.word import Word

from vocabulary.app.search.filter.filter import SearchFilter


# TODO: refactor & test
class DefinitionFilter(SearchFilter):
    def __init__(self, value: str, condition):
        SearchFilter.__init__(self, value)
        self._condition = condition

    def do(self, word: Word):
        word.definitions = self._filter_entries(word.definitions)
        word.idioms = self._filter_entries(word.idioms)

        return word

    def _filter_entries(self, entries: list):
        if entries is None:
            return None

        new_entries = [d for d in entries if self._condition(d)]
        return new_entries
