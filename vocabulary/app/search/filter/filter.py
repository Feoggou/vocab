from vocabulary.domain.word import Word


# TODO: refactor & test
class SearchFilter:
    def __init__(self, value: str):
        self._value = value

    def __eq__(self, other):
        return self._value == other._value and type(self) == type(other)

    def do(self, word: Word):
        raise NotImplementedError()
