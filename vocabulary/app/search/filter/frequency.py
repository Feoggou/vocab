from vocabulary.app.search.filter.filter import SearchFilter

from vocabulary.domain.word import Word


# TODO: refactor & test
class FrequencyFilter(SearchFilter):
    def __init__(self, value: str):
        SearchFilter.__init__(self, value)

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    def _match(self, frequency: str):
        if self._value == "-1" or self._value == "0":
            return frequency == self._value

        return int(frequency) >= int(self._value)

    def do(self, word: Word):
        # TODO: what if not convertible to int?
        if not self._match(word.metadata.frequency):
            word.definitions = []

        return word
