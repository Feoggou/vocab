from vocabulary.app.search.filter.definitions_matcher import DefinitionsMatcher
from vocabulary.app.search.filter.filter import SearchFilter

from vocabulary.domain.word import Word
from vocabulary.domain.entries.derived_forms import DerivedForms


# TODO: refactor & test
class GramValueFilter(SearchFilter):
    def __init__(self, value: str):
        SearchFilter.__init__(self, value)

    def do(self, word: Word):
        word.definitions = self._filter_entries(word.definitions, word.derived_forms)
        word.idioms = self._filter_entries(word.idioms, word.derived_forms)

        return word

    def _filter_entries(self, entries: list, derived_forms: DerivedForms):
        return DefinitionsMatcher(entries).find_matches(self._value, derived_forms)
