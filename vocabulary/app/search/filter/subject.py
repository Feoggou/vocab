from vocabulary.app.search.filter.definition import DefinitionFilter


# TODO: refactor & test
class SubjectFilter(DefinitionFilter):
    def __init__(self, value: str):
        DefinitionFilter.__init__(self, value, lambda definition: self._value in definition.subjects)
