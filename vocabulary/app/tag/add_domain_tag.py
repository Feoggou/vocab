from vocabulary.app.tag.add_basic_tag import AddBasicTag

from vocabulary.app.tag.domain_catalog import DomainCatalog
from vocabulary.utils.errors import *


# TODO: refactor & test
class AddDomainTag(AddBasicTag):
    def __init__(self, tag: str):
        AddBasicTag.__init__(self, tag, DomainCatalog())

    def _ensure_tag_not_in(self, catalog_items: list):
        if self._new_tag in catalog_items:
            raise ItemAlreadyExistsError(self._new_tag)
