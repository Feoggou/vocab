from vocabulary.app.tag.add_tag import AddTag

from vocabulary.app.tag.parent_catalog import ParentCatalog, ParentHierarchy
from vocabulary.utils.errors import *


# TODO: refactor & test
class AddParentTag(AddTag):
    def __init__(self, tag: str):
        AddTag.__init__(self, tag, ParentCatalog())

    def _ensure_tag_not_in(self, catalog_items: list):
        parent = ParentHierarchy(self._new_tag).last_parent

        for cur_item in catalog_items:
            existing_parent = ParentHierarchy(cur_item).last_parent
            if parent == existing_parent:
                raise ItemAlreadyExistsError(self._new_tag)
