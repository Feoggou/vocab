

# TODO: refactor & test
class TextFile:
    def __init__(self, file_path: str):
        self._file_path = file_path

    def read(self) -> str:
        with open(self._file_path, "r") as file:
            return file.read()

    def write(self, text: str):
        with open(self._file_path, "w") as file:
            return file.write(text)
