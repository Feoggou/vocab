from vocabulary.app.tag.add_tag import AddTag

from vocabulary.utils.errors import *


# TODO: refactor & test
class AddBasicTag(AddTag):
    def _ensure_tag_not_in(self, catalog_items: list):
        if self._new_tag in catalog_items:
            raise ItemAlreadyExistsError(self._new_tag)
