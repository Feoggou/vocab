from vocabulary.app.tag.add_basic_tag import AddBasicTag

from vocabulary.app.tag.subject_catalog import SubjectCatalog
from vocabulary.utils.errors import *


# TODO: refactor & test
class AddSubjectTag(AddBasicTag):
    def __init__(self, tag: str):
        AddBasicTag.__init__(self, tag, SubjectCatalog())

    def _ensure_tag_not_in(self, catalog_items: list):
        if self._new_tag in catalog_items:
            raise ItemAlreadyExistsError(self._new_tag)
