import os
import re

from vocabulary.app.tag.catalog import Catalog

from vocabulary.utils import config


# TODO: refactor & test
class ParentHierarchy:
    def __init__(self, item: str):
        self.parent_hierarchy = item

    @property
    def last_parent(self):
        return self.parent_hierarchy.split("/")[-1]

    def replace(self, old: str, new: str):
        old_hierarchy = self.parent_hierarchy.split("/")
        new_hierarchy = Catalog._replace_item(old_hierarchy, old, new)

        result = "/".join(new_hierarchy)
        return result

    def have_sequence(self, sequence: str) -> bool:
        matcher = re.compile(r'\b{}\b'.format(sequence))

        return matcher.search(self.parent_hierarchy) is not None


# TODO: refactor & test
class ParentCatalog(Catalog):
    def __init__(self):
        file_path = os.path.join(config.OUTPUT_JSON_DIR, ".parent")
        Catalog.__init__(self, file_path)

    @staticmethod
    def _replace_item(items: list, old_name: str, new_name: str):
        new_lines = [ParentHierarchy(line).replace(old_name, new_name) for line in items]
        return new_lines
