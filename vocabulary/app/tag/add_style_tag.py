from vocabulary.app.tag.add_basic_tag import AddBasicTag

from vocabulary.app.tag.style_catalog import StyleCatalog
from vocabulary.utils.errors import *


# TODO: refactor & test
class AddStyleTag(AddBasicTag):
    def __init__(self, tag: str):
        AddBasicTag.__init__(self, tag, StyleCatalog())

    def _ensure_tag_not_in(self, catalog_items: list):
        if self._new_tag in catalog_items:
            raise ItemAlreadyExistsError(self._new_tag)
