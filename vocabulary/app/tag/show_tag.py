from vocabulary.app.tag.tags_result import TagsResult

from vocabulary.app.tag.catalog import Catalog


# TODO: refactor & test
class ShowTag:
    def __init__(self, catalog: Catalog=None):
        self._catalog = catalog

    def get_catalog(self):
        return self._catalog

    def do(self):
        tag_list = self._catalog.read()

        return TagsResult(tag_list)
