from vocabulary.app.tag.catalog import Catalog


# TODO: refactor & test
class AddTag:
    def __init__(self, tag: str, catalog: Catalog=None):
        self._new_tag = tag
        self._catalog = catalog

    def get_catalog(self):
        return self._catalog

    def _ensure_tag_not_in(self, catalog_items: list):
        raise NotImplementedError()

    def do(self):
        items = self._catalog.read()

        self._ensure_tag_not_in(items)

        items.append(self._new_tag)
        items = sorted(items)

        self._catalog.replace_all_with(items)
