from vocabulary.app.tag.tags_result import TagsResult


# TODO: refactor & test
class TagsResultDisplayer:
    def __init__(self, result: TagsResult):
        self._result = result

    def execute(self):
        text = "\n".join(self._result.all())

        print(text, "\n")
