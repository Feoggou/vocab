from vocabulary.app.tag.text_file import TextFile


# TODO: refactor & test
class Catalog:
    def __init__(self, path: str):
        self._file_path = path
        self._file = TextFile(path)

    @property
    def file_path(self):
        return self._file_path

    def _safe_read(self):
        try:
            text = self._file.read()
        except FileNotFoundError:
            return ""

        return text

    def read(self):
        text = self._safe_read()

        return [x for x in text.split("\n") if x is not ""]

    def replace_all_with(self, new_items: list):
        text = "\n".join(new_items)
        text += "\n"

        self._file.write(text)

    def update_with(self, new_and_existing: list):
        existing_values = self.read()

        updated_items = list(set(new_and_existing) | set(existing_values))
        updated_items = sorted(updated_items)

        if existing_values != updated_items:
            self.replace_all_with(updated_items)

    @staticmethod
    def _replace_item(items: list, old_name: str, new_name: str):
        new_items = [x if x != old_name else new_name for x in items]

        return new_items

    def rename_item(self, old_name: str, new_name: str):
        items = self.read()

        new_items = self._replace_item(items, old_name, new_name)

        self.replace_all_with(new_items)


# TODO: refactor & test
class NullCatalog(Catalog):
    def __init__(self):
        Catalog.__init__(self, "")

    def read(self):
        return []

    def replace_all_with(self, new_items: list):
        pass

    def rename_item(self, old_name: str, new_name: str):
        pass
