from vocabulary.domain.entries.metadata import Metadata, MetadataReader
from vocabulary.domain.entries.derived_forms import DerivedForms, DerivedFormsReader
from vocabulary.domain.entries.definition import Definition, DefinitionReader
from vocabulary.domain.entries.idiom import Idiom, IdiomReader

from vocabulary.domain.word import Word

from vocabulary.utils.utilities import ConsolePrinter


# TODO: refactor & test
class WordCreator:
    """
    UseCase class: Given a word, read all info about it and persist it.
    """
    def __init__(self, root_word: str):
        self._root_word = root_word

    def execute(self):
        word = self._read_details()
        self._save_json(word)

        return word

    def _read_details(self):
        word = Word(self._root_word, definitions=[], idioms=[])

        self._read_specific_details(word)

        word.metadata = MetadataReader.read()

        ConsolePrinter.output_message("Derived Forms...")
        word.derived_forms = DerivedFormsReader.read()

        return word

    def _read_specific_details(self, word):
        raise NotImplementedError()

    @staticmethod
    def _save_json(word: Word):
        # TODO: I want to save root_word in the json file also
        word.save()


# TODO: refactor & test
class DefinitionCreator(WordCreator):
    def __init__(self, root_word: str):
        WordCreator.__init__(self, root_word)

    def _read_specific_details(self, word: Word):
        ConsolePrinter.output_message("\nAdding Definition...\n")
        word.definitions = [DefinitionReader.read()]


# TODO: refactor & test
class IdiomCreator(WordCreator):
    def __init__(self, root_word: str):
        WordCreator.__init__(self, root_word)

    def _read_specific_details(self, word: Word):
        ConsolePrinter.output_message("\nAdding Idiom...")
        word.idioms = [IdiomReader.read()]


# TODO: refactor & test
class BareWordCreator(WordCreator):
    def __init__(self, root_word: str):
        WordCreator.__init__(self, root_word)

    def _read_specific_details(self, word: Word):
        pass
