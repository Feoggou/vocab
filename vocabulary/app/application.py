from vocabulary.domain.word import Word
from vocabulary.app.word.displayer import WordDisplayer

from vocabulary.app.search.result import SearchResult
from vocabulary.app.search.result_displayer import SearchResultDisplayer

from vocabulary.app.tag.tags_result import TagsResult
from vocabulary.app.tag.tags_result_displayer import TagsResultDisplayer

from vocabulary.cli.parser.cmd_builder import CommandBuilder


def create_displayer(result):
    displayer = None

    if isinstance(result, Word):
        displayer = WordDisplayer(result)
    elif isinstance(result, SearchResult):
        displayer = SearchResultDisplayer(result)
    elif isinstance(result, TagsResult):
        displayer = TagsResultDisplayer(result)

    return displayer


class Application:
    """
    Responsibility: Read an input from stdin, operate on it, and display a result.

    Class kind: UseCase / Entity. It embodies the entire application.
    """

    def __init__(self, builder: CommandBuilder, make_displayer: callable):
        self._builder = builder
        self._make_displayer_for = make_displayer

    def _display(self, result):
        displayer = self._make_displayer_for(result)
        displayer.execute()

    def process(self, input_text: str):
        cmd_object = self._builder.build_from_line(input_text)
        result = cmd_object.execute()

        if result:
            self._display(result)
