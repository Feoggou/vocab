from vocabulary.utils import config

from vocabulary.domain.word import Word
from vocabulary.app.tag.parent_catalog import ParentHierarchy
from vocabulary.domain.entries.definition import Definition


# TODO: refactor & test
class WordDisplayer:
    def __init__(self, word: Word):
        self._word_name = word.word_name
        self._metadata = word.metadata
        self._derived_forms = word.derived_forms
        self._definitions = word.definitions
        self._idioms = word.idioms

    @staticmethod
    def makestr(value):
        return str(value) if value is not None else "<?>"

    @staticmethod
    def output_colored(value, color):
        print(color + WordDisplayer.makestr(value) + config.COLOR_RESET)

    @staticmethod
    def output_if_not_none(prefix: str, value, suffix: str=""):
        if value is not None:
            print(prefix + WordDisplayer.makestr(value) + suffix)

    @staticmethod
    def output_explanations(explanations):
        formatted_explanations = ["o) " + WordDisplayer.makestr(x) for x in explanations]
        print("\n".join(formatted_explanations))

    @staticmethod
    def output_examples(examples: list):
        formatted_examples = ["- " + WordDisplayer.makestr(x) for x in examples]
        if len(formatted_examples):
            print("\n".join(formatted_examples))

    @staticmethod
    def output_parents(parents: list):
        if len(parents) == 0:
           return

        parents_text = "; ".join([ParentHierarchy(item).last_parent for item in parents])

        WordDisplayer.output_colored("[parents: {}]".format(parents_text), color=config.COLOR_YELLOW)

    @staticmethod
    def output_styles(styles: list):
        if len(styles) == 0:
            return

        styles_text = "; ".join(styles)

        WordDisplayer.output_colored("[styles: {}]".format(styles_text), color=config.COLOR_YELLOW)

    @staticmethod
    def output_objects(objects: list):
        if len(objects) == 0:
            return

        objects_text = "; ".join(objects)

        WordDisplayer.output_colored("[objects: {}]".format(objects_text), color=config.COLOR_YELLOW)

    @staticmethod
    def output_subjects(subjects: list):
        if len(subjects) == 0:
            return

        subjects_text = "; ".join(subjects)

        WordDisplayer.output_colored("[subjects: {}]".format(subjects_text), color=config.COLOR_YELLOW)

    @staticmethod
    def output_domains(domains: list):
        if len(domains) == 0:
            return

        domains_text = "; ".join(domains)

        WordDisplayer.output_colored("[domains: {}]".format(domains_text), color=config.COLOR_YELLOW)

    def print_definition(self, definition: Definition):
        self.output_colored(definition.gram_value, config.COLOR_BOLDRED)

        self.output_parents(definition.parents)
        self.output_styles(definition.styles)
        self.output_subjects(definition.subjects)
        self.output_objects(definition.objects)
        self.output_domains(definition.domains)

        self.output_explanations(definition.explanations)
        self.output_examples(definition.examples)

        self.output_if_not_none("\nNOTE: ", definition.notes)

        print("\n")

    def print_idiom(self, idiom):
        self.output_colored(idiom.gram_value, config.COLOR_BOLDRED)

        self.output_parents(idiom.parents)
        self.output_styles(idiom.styles)
        self.output_subjects(idiom.subjects)
        self.output_objects(idiom.objects)
        self.output_domains(idiom.domains)

        self.output_colored(idiom.expression + ": ", config.COLOR_BLUE)
        self.output_explanations(idiom.explanations)

        self.output_examples(idiom.examples)

        self.output_if_not_none("\nNOTE: ", idiom.notes)

        print("\n")

    def output_metadata(self):
        self.output_if_not_none("[origin = ", self._metadata.origin, "]")
        print("[frequency = " + self.makestr(self._metadata.frequency) + "/5]\n")

    def execute(self):
        print("\n")
        self.output_colored(self._word_name, config.COLOR_BOLDBLACK)

        self.output_metadata()

        # DEFINITIONS here
        if self._definitions is not None and len(self._definitions):
            for definition in self._definitions:
                self.print_definition(definition)

        # IDIOMS here
        if self._idioms is not None and len(self._idioms):
            self.output_colored("Idioms", config.COLOR_BOLDBLACK)

            for definition in self._idioms:
                self.print_idiom(definition)
