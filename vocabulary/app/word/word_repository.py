from vocabulary.app.word.io import WordIO
from vocabulary.app.word.iterator import WordIterator
from vocabulary.utils.cache import WordCache


# TODO: refactor & test
class WordRepository:
    """
    Can access (load, store, search through, etc.) and operate upon persisted Word objects.
    """
    _instance = None

    def __init__(self, cache: WordCache):
        self._cache = cache
        self._io = WordIO()

    def __iter__(self):
        iterator = WordIterator()
        return iterator

    def have_word(self, word: str) -> bool:
        return word in self._cache.all

    def rename_tag(self, what: str, old_name: str, new_name: str):
        iterator = iter(self)

        for word in next(iterator):
            new_word = word.rename_tags(what, old_name, new_name)
            if new_word != word:
                self._io.save(new_word)

        return None
