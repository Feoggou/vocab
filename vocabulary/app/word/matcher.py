from vocabulary.domain.word import Word

from vocabulary.app.search.method import SearchMethod


# TODO: refactor & test
# TODO: REVIEW
class DefinitionContentMatcher:
    def __init__(self, found_word: Word, search_method: SearchMethod):
        self._word = found_word
        self._search_method = search_method

    def find_matches(self):
        matches = []

        matches += self._find_matches_in_entries(self._word.definitions)
        matches += self._find_matches_in_entries(self._word.idioms)
        matches += self._find_matches_in_idiom_expression(self._word.idioms)

        return list(set(matches))

    def _find_matches_in_entries(self, entries):
        if entries is None:
            return []

        return self._search_method.find_matches_in_entries(entries)

    def _find_matches_in_idiom_expression(self, entries):
        if entries is None:
            return []

        return self._search_method.find_matches_in_idiom_expression(entries)
