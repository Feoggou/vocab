from vocabulary.domain.word import Word

from vocabulary.domain.entries.definition import Definition, DefinitionReader
from vocabulary.domain.entries.idiom import Idiom

from vocabulary.utils.utilities import ConsolePrinter


# TODO: refactor & test
class WordUpdater:
    def __init__(self, name):
        self._word_name = name

    def _create_entry(self):
        raise NotImplementedError()

    def _load_word(self) -> Word:
        return Word.load(self._word_name)

    def _save_word(self, word: Word):
        word.save()

    def _update_word(self, word: Word, entry):
        raise NotImplementedError()

    def execute(self):
        word = self._load_word()
        entry = self._create_entry()

        self._update_word(word, entry)
        self._save_word(word)

        return word


# TODO: refactor & test
class DefinitionUpdater(WordUpdater):
    def __init__(self, name):
        WordUpdater.__init__(self, name)

    def _create_entry(self) -> Definition:
        ConsolePrinter.output_message("Adding Definition...")
        return DefinitionReader.read()

    def _update_word(self, word: Word, entry):
        if word.definitions is None:
            word.definitions = [entry]
        else:
            word.definitions.append(entry)


class IdiomUpdater(WordUpdater):
    def __init__(self, name):
        WordUpdater.__init__(self, name)

    def _create_entry(self) -> Idiom:
        ConsolePrinter.output_message("Adding Idiom...")

        return Idiom.read()

    def _update_word(self, word: Word, entry):
        if word.idioms is None:
            word.idioms = [entry]
        else:
            word.idioms.append(entry)