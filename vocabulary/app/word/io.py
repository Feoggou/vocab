import json
import os

from vocabulary.app.word.file import WordFile
from vocabulary.domain.word import Word
from vocabulary.utils import config


# TODO: Or, WordStorage, WordStore
class WordIO:
    @staticmethod
    def load(word_name: str) -> Word:
        file_path = Word.compute_path(word_name)

        json_object = WordFile(file_path).read()
        return Word.from_json(json_object)

    @classmethod
    def save(cls, word: Word):
        json_object = word.to_json()
        file_name = word.word_name + ".word"
        full_name = os.path.join(config.OUTPUT_JSON_DIR, file_name)

        with open(full_name, "w", encoding="utf-8") as f:
            json.dump(json_object, f, indent=4, sort_keys=True, ensure_ascii=False)
