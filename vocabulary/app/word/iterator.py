from vocabulary.app.word.io import WordIO
from vocabulary.utils import utilities
from vocabulary.domain.word import Word


# TODO: refactor & test
class WordIterator:
    def __init__(self):
        self._io = WordIO()

    def __next__(self) -> Word:
        for word_name in utilities.get_vocab_words():
            word = self._io.load(word_name)
            yield word
