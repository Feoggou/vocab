import json
import os

from vocabulary.utils.errors import WordNotFoundError


# TODO: refactor & test
class WordFile:
    def __init__(self, file_path: str):
        self._file_path = file_path

    def read(self):
        if not os.path.isfile(self._file_path):
            raise WordNotFoundError(self._file_path)

        with open(self._file_path, "r", encoding="utf-8") as f:
            json_obj = json.load(f)

        return json_obj
