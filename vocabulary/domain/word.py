from dataclasses import dataclass
from typing import List

from vocabulary.domain.entries.metadata import Metadata
from vocabulary.domain.entries.derived_forms import DerivedForms

from vocabulary.domain.entries.definition import Definition
from vocabulary.domain.entries.idiom import Idiom
from vocabulary.app.word.file import WordFile

from vocabulary.utils import config

import copy
import os
import json


@dataclass
class DefinitionList:
    definitions: List[Definition]

    def rename_tags(self, what: str, old_parent: str, new_parent: str) -> list:
        if self.definitions is None:
            return []

        new_definitions = [definition.rename_tags(what, old_parent, new_parent) for definition in self.definitions]
        return new_definitions


# TODO: refactor & test
@dataclass
class Word:
    root_word: str
    definitions: List[Definition]=None
    idioms: List[Idiom]=None
    metadata: Metadata = None
    derived_forms: DerivedForms = None

    @property
    def word_name(self):
        return self.root_word

    # TODO: perhaps dataclass supports this
    def to_json(self):
        json_object = {
            "root_word": self.word_name,
            "metadata": copy.deepcopy(vars(self.metadata)),
            "derived_forms": copy.deepcopy(vars(self.derived_forms)),
            "definitions": [copy.deepcopy(vars(x)) for x in self.definitions],
            "idioms": [copy.deepcopy(x.__dict__) for x in self.idioms]
        }

        return json_object

    # TODO: perhaps dataclass supports this
    @staticmethod
    def from_json(json_object: dict):
        return Word(
            root_word=json_object["root_word"],
            metadata=Metadata(**json_object["metadata"]),
            derived_forms=DerivedForms(**json_object["derived_forms"]),
            definitions=[Definition(**item) for item in json_object.get("definitions", [])],
            idioms=[Idiom(**item) for item in json_object.get("idioms", [])]
        )

    @staticmethod
    def compute_path(word_name):
        file_name = word_name + ".word"
        file_path = os.path.join(config.OUTPUT_JSON_DIR, file_name)

        return file_path

    def rename_tags(self, what: str, old_name: str, new_name: str):
        new_word = copy.deepcopy(self)

        definition_list = DefinitionList(self.definitions)
        idiom_list = DefinitionList(self.idioms)

        new_word.definitions = definition_list.rename_tags(what, old_name, new_name)
        new_word.idioms = idiom_list.rename_tags(what, old_name, new_name)


class WordEditor:
    pass


class WordPersistence:
    pass
