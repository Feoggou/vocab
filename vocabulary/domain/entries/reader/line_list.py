from vocabulary.domain.entries.reader.item import ItemReader


# TODO: refactor & test
class LineListReader(ItemReader):
    def __init__(self, msg):
        ItemReader.__init__(self, msg)

    def read(self):
        value = super().read()

        # if input read is "" (translated by ItemReader as None), then return empty list
        # --- so that it won't throw exception below.
        if value is None:
            return []

        items = value.split(", ")

        return items
