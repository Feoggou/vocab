from vocabulary.domain.entries.reader.marker_reader import MarkerReader


# TODO: refactor & test
class UsefulnessReader(MarkerReader):
    def __init__(self, msg):
        MarkerReader.__init__(self, msg, {
            "-1": "I'm clueless",
            "0": "Useless",
            "1": "It might fit somewhere",
            "2": "I could use it",
            "3": "I'd like to use it",
            "4": "I SHOULD use this",
            "5": "I MUST use this"
        })
