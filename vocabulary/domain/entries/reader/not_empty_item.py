from vocabulary.domain.entries.reader.item import ItemReader


# TODO: refactor & test
class NotEmptyItemReader(ItemReader):
    def __init__(self, msg):
        ItemReader.__init__(self, msg)

    @staticmethod
    def output_valid():
        print("\nNOTE: value must not be none")

    def read(self):
        value = None
        while value is None:
            self.output_valid()
            value = super().read()

        return value
