from vocabulary.domain.entries.reader.marker_reader import MarkerReader


# TODO: refactor & test
class FrequencyReader(MarkerReader):
    def __init__(self, msg):
        MarkerReader.__init__(self, msg, {
            "-1": "Have no idea (not mentioned?)",
            "0": "Unused (i.e. arhaic)",
            "1": "1/5 (Used rarely - in bottom 50% of used)",
            "2": "2/5 (Used occasionally - in top 30K)",
            "3": "3/5 (Common - in top 10K)",
            "4": "4/5 (Very common - in top 4K)",
            "5": "5/5 (Extremely common - in top 1K)"
        })
