from vocabulary.domain.entries.reader.item_reader_with_set_of_possible_values import ItemReaderWithSetOfPossibleValues


# TODO: refactor & test
class GramValueReader(ItemReaderWithSetOfPossibleValues):
    def __init__(self, msg):
        ItemReaderWithSetOfPossibleValues.__init__(self, msg, [
            "noun", "art", "adj", "pron", "num", "vt", "vi", "vb", "adv", "prep", "conj", "interj"
        ])

    def output_valid(self):
        print("Either of - ", ", ".join(self._valid_values))
