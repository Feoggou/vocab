from vocabulary.domain.entries.reader.reader import Reader


# TODO: refactor & test
class ItemReader(Reader):
    def __init__(self, msg):
        self.msg = msg + ": "

    def read(self):
        value = input(self.msg)
        return value if len(value) else None
