from vocabulary.domain.entries.reader.item_reader_with_set_of_possible_values import ItemReaderWithSetOfPossibleValues

from vocabulary.utils.utilities import ConsolePrinter


# TODO: refactor & test
class DerivedFormsItemReader(ItemReaderWithSetOfPossibleValues):
    def __init__(self, msg):
        # TODO: None is not in the list of possible values, and was not tested against
        ItemReaderWithSetOfPossibleValues.__init__(self, msg, ["yes", "y", "no", "n"])

    def output_valid(self):
        message = "valid values are: " + ", ".join(self._valid_values)
        ConsolePrinter.output_message(message)

    def read(self):
        value = super().read()

        if value == "y":
            return "yes"

        if value == "n" or value is None:
            return "no"

        return value
