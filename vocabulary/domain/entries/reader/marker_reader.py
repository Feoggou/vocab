from vocabulary.domain.entries.reader.item_reader_with_set_of_possible_values import ItemReaderWithSetOfPossibleValues


# TODO: refactor & test
class MarkerReader(ItemReaderWithSetOfPossibleValues):
    def __init__(self, msg, valid_values: dict):
        ItemReaderWithSetOfPossibleValues.__init__(self, msg, valid_values)

    def output_valid(self):
        valid_values = ["{} - {}".format(x, self._valid_values[x]) for x in self._valid_values]

        print("\nValid values: ")
        print("\n".join(valid_values))
