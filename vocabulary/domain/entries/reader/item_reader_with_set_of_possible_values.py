from vocabulary.domain.entries.reader.item import ItemReader


# TODO: refactor & test
class ItemReaderWithSetOfPossibleValues(ItemReader):
    def __init__(self, msg, valid_values):
        ItemReader.__init__(self, msg)
        self._valid_values = valid_values

    def output_valid(self):
        raise NotImplementedError()

    def is_valid(self, value: str):
        return value in self._valid_values

    def read(self):
        value = None

        while not self.is_valid(value):
            self.output_valid()

            value = super().read()

        return value
