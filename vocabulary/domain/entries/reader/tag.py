from vocabulary.domain.entries.reader.line_list import LineListReader

from vocabulary.app.tag.catalog import Catalog


# TODO: refactor & test
class TagReader(LineListReader):
    def __init__(self, msg, catalog: Catalog):
        LineListReader.__init__(self, msg)
        self._catalog = catalog

    def get_catalog(self):
        return self._catalog

    @staticmethod
    def output_existing(valid_values: list):
        print("\nExisting items:\n")
        print("\n".join(valid_values))
        print("")

    def read(self):
        existing_values = self._catalog.read()

        self.output_existing(existing_values)

        read_items = super().read()

        self._catalog.update_with(read_items)

        return read_items



