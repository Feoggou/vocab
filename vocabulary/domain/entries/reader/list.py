from vocabulary.domain.entries.reader.reader import Reader


# TODO: refactor & test
class ListReader(Reader):
    def __init__(self, msg, reader: Reader):
        self._msg = msg + "... "
        self._reader = reader

    @staticmethod
    def is_valid(value: str):
        return value is not None

    def read(self):
        items = []

        while True:
            value = self._reader.read()
            if not self.is_valid(value):
                break

            items.append(value)

        return items


# TODO: unit test
class NotEmptyListReader(ListReader):
    def read(self):
        items = []

        while len(items) == 0:
            items = super().read()

        return items
