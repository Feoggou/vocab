from dataclasses import dataclass, asdict

from vocabulary.domain.entries.reader.not_empty_item import NotEmptyItemReader

from ..entries.definition import Definition, DefinitionReader


@dataclass
class Idiom(Definition):
    expression: str

    @staticmethod
    def from_definition(expression: str, definition: Definition):
        return Idiom(expression=expression, **asdict(definition))


class IdiomReader:
    @staticmethod
    def read() -> Idiom:
        definition = DefinitionReader.read()
        return Idiom.from_definition(expression=NotEmptyItemReader("Expression").read(), definition=definition)
