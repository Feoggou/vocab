from dataclasses import dataclass

from vocabulary.domain.entries.reader.item import ItemReader
from vocabulary.domain.entries.reader.frequency import FrequencyReader


@dataclass(frozen=True)
class Metadata:
    origin: str
    frequency: int


class MetadataReader:
    @staticmethod
    def read() -> Metadata:
        return Metadata(
            origin=ItemReader("Origin").read(),
            frequency=FrequencyReader("Frequency").read()
        )
