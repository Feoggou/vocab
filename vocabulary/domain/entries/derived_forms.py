from dataclasses import dataclass
from vocabulary.domain.entries.reader.derived_forms_item_reader import DerivedFormsItemReader


@dataclass(frozen=True)
class DerivedForms:
    noun: str = "no"
    adj: str = "no"
    verb: str = "no"
    adv: str = "no"
    others: str = "no"

    def have(self, gram_value: str) -> bool:
        return {
            "noun": self.noun == "yes",
            "adj": self.adj == "yes",
            "verb": self.verb == "yes",
            "adv": self.adv == "yes",
            "others": self.others == "yes",
        }.get(gram_value, False)


class DerivedFormsReader:
    @classmethod
    def read(cls):
        return DerivedForms(
            noun=cls._read_item("Have noun"),
            adj=cls._read_item("Have adj"),
            verb=cls._read_item("Have verb"),
            adv=cls._read_item("Have adv"),
            others=cls._read_item("Have other"),
        )

    @staticmethod
    def _read_item(prompt: str):
        return DerivedFormsItemReader(prompt).read()
