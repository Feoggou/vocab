from dataclasses import dataclass
from typing import List

from vocabulary.domain.entries.reader.list import ListReader, NotEmptyListReader
from vocabulary.domain.entries.reader.item import ItemReader

from vocabulary.domain.entries.reader.gram_value import GramValueReader
from vocabulary.domain.entries.reader.usefulness import UsefulnessReader
from vocabulary.domain.entries.reader.domain import DomainReader
from vocabulary.domain.entries.reader.parent import ParentReader
from vocabulary.domain.entries.reader.style import StyleReader
from vocabulary.domain.entries.reader.subject import SubjectReader
from vocabulary.domain.entries.reader.object import ObjectReader

import copy


@dataclass
class Definition:
    gram_value: str
    explanations: List[str]
    examples: List[str]
    domains: List[str]
    subjects: List[str]
    objects: List[str]
    parents: List[str]
    notes: str
    usefulness: int
    styles: List[str]

    @staticmethod
    def _replace(container: list, old, new):
        return [x if x != old else new for x in container]

    def rename_tags(self, tags_name: str, old: str, new: str):
        new_definition = copy.deepcopy(self)

        new_items = self._replace(getattr(self, tags_name), old, new)
        setattr(new_definition, tags_name, new_items)

        return new_definition


# TODO:
class DefinitionEditor:
    pass


class DefinitionReader:
    @staticmethod
    def read() -> Definition:
        return Definition(
            subjects=SubjectReader("Subjects").read(),
            objects=ObjectReader("Objects ('is what' - not the subject, but objects)").read(),
            parents=ParentReader("Parent / Root words (answers: 'is a')").read(),
            domains=DomainReader("Domain (answers: 'in the context of')").read(),
            styles=StyleReader("Styles ('is how')").read(),

            gram_value=GramValueReader("Gram Value").read(),
            explanations=NotEmptyListReader("Explanations", ItemReader("Explanation")).read(),
            examples=ListReader("Examples", ItemReader("Example")).read(),

            notes=ItemReader("Add Notes").read(),
            usefulness=UsefulnessReader("How useful you think this def will be").read()
        )
