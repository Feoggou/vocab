

# TODO: refactor & test
class Readable:
    def __init__(self):
        pass

    def __str__(self):
        """Convert this Readable struct into a string representation

        Like, {
        a=4
        b=3
        c="something"
        d=["yes", "no"]
        }
        """
        raise NotImplementedError()

    def __eq__(self, other):
        """Compare the contents of two Readables

        Make sure that it compares contents, not references (e.g. list items, not list references)
        """
        raise NotImplementedError()

    @staticmethod
    def read():
        """Creates the Readable object by reading it from CLI."""
        raise NotImplementedError()

    @staticmethod
    def init_data_member(kwargs, keyname: str, default_value):
        return default_value if keyname not in kwargs.keys() else kwargs[keyname]