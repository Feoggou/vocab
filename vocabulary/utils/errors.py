

class QuitException(Exception):
    pass


class CommandNotFoundError(Exception):
    def __init__(self, cmd_name: str):
        self.cmd_name = cmd_name


class WordNotFoundError(Exception):
    def __init__(self, word_name: str):
        self.word_name = word_name


class BadlyFormattedCommand(Exception):
    def __init__(self, *args, **kwargs):
        Exception(args, kwargs)


class ItemAlreadyExistsError(Exception):
    def __init__(self, *args, **kwargs):
        Exception(args, kwargs)


# TODO: WordNotFound (for display)
