import os
from vocabulary.utils import config


# NOTE: Could use a WordRepository and a WordIterator, but perhaps it would be over-engineering.
# TODO: remove function
def get_vocab_words() -> list:
    return sorted([x.replace(".word", "") for x in os.listdir(config.OUTPUT_JSON_DIR) if x.endswith(".word")])


# TODO: refactor & test
class ConsolePrinter:
    @staticmethod
    def output(arg, *args):
        print(arg, *args)

    @staticmethod
    def output_message(message: str):
        if len(message):
            print(message)
