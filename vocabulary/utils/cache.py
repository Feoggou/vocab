from pathlib import Path
from typing import List


# TODO: refactor & test
class WordCache:
    def __init__(self, words_location: Path):
        self._all_words = None
        self._location = words_location

    @property
    def all(self) -> List[str]:
        if self._all_words is None:
            self._reload()
        return self._all_words

    def _reload(self):
        self._all_words = sorted(self._fetch_all_files_by_extension(self._location, extension=".word"))

    @staticmethod
    def _fetch_all_files_by_extension(location: Path, extension: str):
        import os
        return [x.replace(extension, "") for x in os.listdir(str(location)) if x.endswith(extension)]
