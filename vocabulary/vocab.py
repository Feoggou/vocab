import os

from vocabulary.app.application import create_displayer
from vocabulary.app.word.word_repository import WordRepository
from vocabulary.cli.parser.cmd_builder import CommandBuilder
from vocabulary.cli.parser.cmd_finder import CommandFinder
from vocabulary.cli.parser.cmd_parser import CommandParser
from vocabulary.utils import config
from vocabulary.utils.cache import WordCache


def setup_readline():
    import readline
    import atexit

    readline.parse_and_bind("tab: complete")
    history_file = os.path.join(os.environ['HOME'], '.vocabhistory')

    try:
        readline.read_history_file(history_file)
    except IOError:
        pass

    atexit.register(readline.write_history_file, history_file)


def setup_json_dir():
    from vocabulary.utils import config
    from pathlib import Path

    if __name__ == "__main__":
        project_path = Path(__file__).parent.parent
        config.OUTPUT_JSON_DIR = project_path.joinpath("vocab_files")
    else:
        config.OUTPUT_JSON_DIR = Path.home().joinpath("vocab-files")
        if not os.path.exists(config.OUTPUT_JSON_DIR):
            os.makedirs(config.OUTPUT_JSON_DIR)
    print("\nThe word data will be saved in the directory: \n", config.OUTPUT_JSON_DIR, "\n")


def run_app():
    from vocabulary.app.application import Application
    from vocabulary.utils.errors import WordNotFoundError, CommandNotFoundError, QuitException

    setup_readline()
    setup_json_dir()

    cache = WordCache(words_location=config.OUTPUT_JSON_DIR)
    command_builder = CommandBuilder(repo=WordRepository(cache), finder=CommandFinder(), parser=CommandParser())

    app = Application(command_builder, create_displayer)

    try:
        while True:
            try:
                input_text = input("vocab> ")
                if len(input_text.strip()):
                    app.process(input_text)
            except WordNotFoundError as e:
                print("Sorry, word '{}' was not found. You should add it.".format(e.word_name))
            except CommandNotFoundError as e:
                print("Unknown command: '{}'.".format(e.cmd_name))
            except KeyboardInterrupt:
                print("\n Operation Canceled.")

    except QuitException:
        print("\nBye-bye!")


if __name__ == "__main__":
    run_app()

