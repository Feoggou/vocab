import unittest

from vocabulary.app.tag.parent_catalog import ParentHierarchy


class ParentHierarchyTest(unittest.TestCase):
    def test_haveSequence(self):
        hierarchy = ParentHierarchy("a/b/c/d")

        result = hierarchy.have_sequence("a/b")

        self.assertTrue(result)

    def test_haveSequence_atTheEnd(self):
        hierarchy = ParentHierarchy("a/b/c/d")

        result = hierarchy.have_sequence("c/d")

        self.assertTrue(result)

    def test_haveOne(self):
        hierarchy = ParentHierarchy("what/you/have/here")

        result = hierarchy.have_sequence("you")

        self.assertTrue(result)

    def test_sparseItems_dontHaveSequence(self):
        hierarchy = ParentHierarchy("a/b/c/d")

        result = hierarchy.have_sequence("a/c")

        self.assertFalse(result)

    def test_containsString_dontHaveSequence(self):
        hierarchy = ParentHierarchy("here/now/then/d")

        result = hierarchy.have_sequence("here/no")

        self.assertFalse(result)

    def test_stringContainsOne_dontHaveSequence(self):
        hierarchy = ParentHierarchy("whatsoever/now/then/d")

        result = hierarchy.have_sequence("what")

        self.assertFalse(result)


if __name__ == '__main__':
    unittest.main()
