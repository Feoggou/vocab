import unittest
import os

from vocabulary.utils import config
from vocabulary.app.tag.style_catalog import StyleCatalog


class StyleCatalogTest(unittest.TestCase):
    def test_styleCatalog_pointsTo_correctFileName(self):
        catalog = StyleCatalog()

        expected_file_name = os.path.join(config.OUTPUT_JSON_DIR, ".style")

        self.assertEqual(catalog.file_path, expected_file_name)


if __name__ == '__main__':
    unittest.main()
