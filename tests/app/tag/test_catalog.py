import unittest
from unittest.mock import patch

from vocabulary.app.tag.catalog import Catalog
from vocabulary.app.tag.text_file import TextFile


class CatalogTest(unittest.TestCase):
    def setUp(self):
        self.catalog = Catalog("<filename>")

    def test_read_catalog(self):
        with patch.object(TextFile, "read") as mock_read:
            mock_read.return_value = "a\nb\nc\n"

            items = self.catalog.read()

        self.assertEqual(items, ["a", "b", "c"])

    def test_whenCatalogDoesNotExist_read_returnsEmpty(self):
        with patch.object(TextFile, "read") as mock_read:
            mock_read.side_effect = FileNotFoundError()

            items = self.catalog.read()

        self.assertEqual(items, [])

    def test_replace_with(self):
        with patch.object(TextFile, "write") as mock_write:
            self.catalog.replace_all_with(["a", "b", "c"])

        mock_write.assert_called_once_with("a\nb\nc\n")

    def test_rename_item(self):
        old_items = ["a", "b", "old", "c"]
        expected_new_items = ["a", "b", "new", "c"]

        with patch.object(self.catalog, "read", autospec=True) as mock_read:
            mock_read.return_value = old_items

            with patch.object(self.catalog, "replace_all_with", autospec=True) as mock_replace:
                self.catalog.rename_item("old", "new")

        mock_read.assert_called_once()
        mock_replace.assert_called_once_with(expected_new_items)


class CatalogUpdateTest(unittest.TestCase):
    def setUp(self):
        self.catalog = Catalog("<filename>")

        self.patcher_read = patch.object(self.catalog, "read", autospec=True)
        self.patcher_replace = patch.object(self.catalog, "replace_all_with", autospec=True)

        self.mock_read = self.patcher_read.start()
        self.mock_replace = self.patcher_replace.start()

    def tearDown(self):
        self.patcher_read.stop()
        self.patcher_replace.stop()

    def test_whenItemsAreBothExistingAndNew_updatesWithUniques(self):
        catalog_items = ["a", "b", "c"]
        input_items = ["a", "d", "c", "e"]
        self.mock_read.return_value = catalog_items

        self.catalog.update_with(input_items)

        self.mock_replace.assert_called_once_with(["a", "b", "c", "d", "e"])

    def test_whenAllItemsExist_updateDoesNotCallReplace(self):
        catalog_items = ["a", "b", "c"]
        input_items = ["a", "c"]
        self.mock_read.return_value = catalog_items

        self.catalog.update_with(input_items)

        self.mock_replace.assert_not_called()

    def test_whenItemsAreUnordered_updateAsOrdered(self):
        catalog_items = ["b", "d", "f"]
        input_items = ["c", "a", "g", "b", "e"]
        self.mock_read.return_value = catalog_items

        self.catalog.update_with(input_items)

        self.mock_replace.assert_called_once_with(["a", "b", "c", "d", "e", "f", "g"])

    def test_whenCatalogIsEmpty_writeAllItemsAsOrdered(self):
        catalog_items = []
        input_items = ["c", "a", "b"]
        self.mock_read.return_value = catalog_items

        self.catalog.update_with(input_items)

        self.mock_replace.assert_called_once_with(["a", "b", "c"])


if __name__ == '__main__':
    unittest.main()
