import unittest
import os

from vocabulary.utils import config
from vocabulary.app.tag.subject_catalog import SubjectCatalog


class SubjectCatalogTest(unittest.TestCase):
    def test_subjectCatalog_pointsTo_correctFileName(self):
        catalog = SubjectCatalog()

        expected_file_name = os.path.join(config.OUTPUT_JSON_DIR, ".subject")

        self.assertEqual(catalog.file_path, expected_file_name)


if __name__ == '__main__':
    unittest.main()
