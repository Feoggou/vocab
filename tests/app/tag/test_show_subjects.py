import unittest

from vocabulary.app.tag.show_subjects import ShowSubjects
from vocabulary.app.tag.subject_catalog import SubjectCatalog


class ShowSubjectTagsTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        show = ShowSubjects()

        self.assertIsInstance(show.get_catalog(), SubjectCatalog)


if __name__ == "__main__":
    unittest.main()
