import unittest
from unittest.mock import patch
import os

from vocabulary.utils import config
from vocabulary.app.tag.parent_catalog import ParentCatalog


class ParentCatalogTest(unittest.TestCase):
    def test_parentCatalog_pointsTo_correctFileName(self):
        catalog = ParentCatalog()

        expected_file_name = os.path.join(config.OUTPUT_JSON_DIR, ".parent")

        self.assertEqual(catalog.file_path, expected_file_name)

    def test_rename_item_renamesEachInHierarchy(self):
        catalog = ParentCatalog()

        old_items = ["old/b1/c1", "a2/old/c2", "a3/b3/old", "a4/b4/c4"]
        expected_new_items = ["new/b1/c1", "a2/new/c2", "a3/b3/new", "a4/b4/c4"]

        with patch.object(catalog, "read", autospec=True) as mock_read:
            mock_read.return_value = old_items

            with patch.object(catalog, "replace_all_with", autospec=True) as mock_replace:
                catalog.rename_item("old", "new")

        mock_read.assert_called_once()
        mock_replace.assert_called_once_with(expected_new_items)

    def test_rename_whole_item(self):
        catalog = ParentCatalog()

        old_items = ["a", "b", "old", "c"]
        expected_new_items = ["a", "b", "new", "c"]

        with patch.object(catalog, "read", autospec=True) as mock_read:
            mock_read.return_value = old_items

            with patch.object(catalog, "replace_all_with", autospec=True) as mock_replace:
                catalog.rename_item("old", "new")

        mock_read.assert_called_once()
        mock_replace.assert_called_once_with(expected_new_items)


if __name__ == '__main__':
    unittest.main()
