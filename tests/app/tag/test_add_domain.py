import unittest

from vocabulary.app.tag.add_domain_tag import AddDomainTag
from vocabulary.app.tag.domain_catalog import DomainCatalog


class AddDomainTagTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        adder = AddDomainTag("dummy")

        self.assertIsInstance(adder.get_catalog(), DomainCatalog)


if __name__ == "__main__":
    unittest.main()
