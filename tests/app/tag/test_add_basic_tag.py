import unittest
from unittest.mock import patch

from vocabulary.app.tag.add_basic_tag import AddBasicTag

from vocabulary.app.tag.catalog import NullCatalog
from vocabulary.utils.errors import *


class AddBasicTagTest(unittest.TestCase):
    def setUp(self):
        self.patcher_read = patch.object(NullCatalog, "read")
        self.mock_read = self.patcher_read.start()

        self.patcher_replace = patch.object(NullCatalog, "replace_all_with")
        self.mock_replace = self.patcher_replace.start()

    def tearDown(self):
        self.patcher_read.stop()
        self.patcher_replace.stop()

    def test_whenCatalogHasItems_addExistingItem_raisesException(self):
        adder = AddBasicTag("b", NullCatalog())

        old_items = ["a", "b"]
        self.mock_read.return_value = old_items

        with self.assertRaises(ItemAlreadyExistsError):
            adder.do()

        self.mock_replace.assert_not_called()


if __name__ == "__main__":
    unittest.main()
