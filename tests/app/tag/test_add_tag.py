import unittest
from unittest.mock import patch

from vocabulary.app.tag.add_tag import AddTag

from vocabulary.app.tag.catalog import NullCatalog


class AddTagTest(unittest.TestCase):
    def setUp(self):
        self.patcher_read = patch.object(NullCatalog, "read")
        self.mock_read = self.patcher_read.start()

        self.patcher_replace = patch.object(NullCatalog, "replace_all_with")
        self.mock_replace = self.patcher_replace.start()

    def tearDown(self):
        self.patcher_read.stop()
        self.patcher_replace.stop()

    def test_whenCatalogIsEmpty_addNewItem(self):
        adder = AddTag("new_item", NullCatalog())

        self.mock_read.return_value = []

        with patch.object(AddTag, "_ensure_tag_not_in") as mock_not_in:
            adder.do()

        self.mock_replace.assert_called_once_with(["new_item"])
        mock_not_in.assert_called_once()

    def test_whenCatalogHasItems_addNewItem_preservesAlphabeticOrder(self):
        adder = AddTag("bee", NullCatalog())

        old_items = ["a", "b", "c", "d"]
        expected_new_items = ["a", "b", "bee", "c", "d"]
        self.mock_read.return_value = old_items

        with patch.object(AddTag, "_ensure_tag_not_in") as mock_not_in:
            adder.do()

        self.mock_replace.assert_called_once_with(expected_new_items)
        mock_not_in.assert_called_once()


if __name__ == "__main__":
    unittest.main()
