import unittest

from vocabulary.app.tag.show_objects import ShowObjects
from vocabulary.app.tag.object_catalog import ObjectCatalog


class ShowObjectTagsTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        show = ShowObjects()

        self.assertIsInstance(show.get_catalog(), ObjectCatalog)


if __name__ == "__main__":
    unittest.main()
