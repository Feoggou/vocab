import unittest
from unittest.mock import patch

from vocabulary.app.tag.add_parent_tag import AddParentTag

from vocabulary.app.tag.parent_catalog import ParentCatalog
from vocabulary.utils.errors import *


class AddParentTagTest(unittest.TestCase):
    def setUp(self):
        self.patcher_read = patch.object(ParentCatalog, "read")
        self.mock_read = self.patcher_read.start()

        self.patcher_replace = patch.object(ParentCatalog, "replace_all_with")
        self.mock_replace = self.patcher_replace.start()

    def tearDown(self):
        self.patcher_read.stop()
        self.patcher_replace.stop()

    def test_whenCatalogHasItems_addExistingParent_raisesException(self):
        adder = AddParentTag("b")

        old_items = ["a", "b"]
        self.mock_read.return_value = old_items

        with self.assertRaises(ItemAlreadyExistsError):
            adder.do()

        self.mock_replace.assert_not_called()

    def test_whenCatalogHasItems_addHierarcyWithExistingParent_raisesException(self):
        adder = AddParentTag("hierarch/existing")

        old_items = ["hierarch/a", "existing"]
        self.mock_read.return_value = old_items

        with self.assertRaises(ItemAlreadyExistsError):
            adder.do()

        self.mock_replace.assert_not_called()


if __name__ == "__main__":
    unittest.main()
