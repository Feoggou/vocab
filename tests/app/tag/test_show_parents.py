import unittest

from vocabulary.app.tag.show_parents import ShowParent
from vocabulary.app.tag.parent_catalog import ParentCatalog


class ShowParentTagTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        show = ShowParent()

        self.assertIsInstance(show.get_catalog(), ParentCatalog)


if __name__ == "__main__":
    unittest.main()
