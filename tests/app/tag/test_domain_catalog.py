import unittest
import os

from vocabulary.utils import config
from vocabulary.app.tag.domain_catalog import DomainCatalog


class DomainCatalogTest(unittest.TestCase):
    def test_domainCatalog_pointsTo_correctFileName(self):
        catalog = DomainCatalog()

        expected_file_name = os.path.join(config.OUTPUT_JSON_DIR, ".domain")

        self.assertEqual(catalog.file_path, expected_file_name)


if __name__ == '__main__':
    unittest.main()
