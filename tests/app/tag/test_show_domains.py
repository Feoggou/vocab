import unittest

from vocabulary.app.tag.show_domains import ShowDomains
from vocabulary.app.tag.domain_catalog import DomainCatalog


class ShowDomainTagsTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        show = ShowDomains()

        self.assertIsInstance(show.get_catalog(), DomainCatalog)


if __name__ == "__main__":
    unittest.main()
