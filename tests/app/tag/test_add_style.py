import unittest

from vocabulary.app.tag.add_style_tag import AddStyleTag
from vocabulary.app.tag.style_catalog import StyleCatalog


class AddStyleTagTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        adder = AddStyleTag("dummy")

        self.assertIsInstance(adder.get_catalog(), StyleCatalog)


if __name__ == "__main__":
    unittest.main()
