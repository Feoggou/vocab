import unittest
import os

from vocabulary.utils import config
from vocabulary.app.tag.object_catalog import ObjectCatalog


class ObjectCatalogTest(unittest.TestCase):
    def test_objectCatalog_pointsTo_correctFileName(self):
        catalog = ObjectCatalog()

        expected_file_name = os.path.join(config.OUTPUT_JSON_DIR, ".object")

        self.assertEqual(catalog.file_path, expected_file_name)


if __name__ == '__main__':
    unittest.main()
