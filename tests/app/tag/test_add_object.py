import unittest

from vocabulary.app.tag.add_object_tag import AddObjectTag
from vocabulary.app.tag.object_catalog import ObjectCatalog


class AddObjectTagTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        adder = AddObjectTag("dummy")

        self.assertIsInstance(adder.get_catalog(), ObjectCatalog)


if __name__ == "__main__":
    unittest.main()
