import unittest

from vocabulary.app.tag.add_subject_tag import AddSubjectTag
from vocabulary.app.tag.subject_catalog import SubjectCatalog


class AddSubjectTagTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        adder = AddSubjectTag("dummy")

        self.assertIsInstance(adder.get_catalog(), SubjectCatalog)


if __name__ == "__main__":
    unittest.main()
