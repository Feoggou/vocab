import unittest

from vocabulary.app.tag.show_styles import ShowStyles
from vocabulary.app.tag.style_catalog import StyleCatalog


class ShowStyleTagsTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        show = ShowStyles()

        self.assertIsInstance(show.get_catalog(), StyleCatalog)


if __name__ == "__main__":
    unittest.main()
