import unittest
from unittest.mock import patch

from vocabulary.app.tag.show_tag import ShowTag

from vocabulary.app.tag.catalog import Catalog
from vocabulary.app.tag.tags_result import TagsResult


class ShowTagTest(unittest.TestCase):
    def test_show_one_item(self):
        show = ShowTag(Catalog("dummy"))

        expected_result = ["one_item"]

        with patch.object(Catalog, "read") as mock_read:
            mock_read.return_value = expected_result

            result = show.do()

        self.assertIsInstance(result, TagsResult)
        self.assertListEqual(result.all(), expected_result)


if __name__ == '__main__':
    unittest.main()
