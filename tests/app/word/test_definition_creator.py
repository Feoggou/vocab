import unittest
from unittest.mock import patch

from tests.utils import a_definition
from vocabulary.app.operations.create import DefinitionCreator
from vocabulary.domain.word import Word

from vocabulary.domain.entries.metadata import Metadata, MetadataReader
from vocabulary.domain.entries.definition import Definition, DefinitionReader
from vocabulary.domain.entries.derived_forms import DerivedForms, DerivedFormsReader

from vocabulary.utils.utilities import ConsolePrinter


class DefinitionCreatorTest(unittest.TestCase):
    def setUp(self):
        self.a_metadata = Metadata(origin='an_origin', frequency=-1)

    @patch.object(DefinitionReader, "read")
    @patch.object(MetadataReader, "read")
    @patch.object(DerivedFormsReader, "read")
    def test_readDetails_calls_wordReaderFor_allDefinitions_and_returnsTheObjects(self,
                                                                                  mock_derived_forms,
                                                                                  mock_metadata,
                                                                                  mock_definition_reader):
        root_word = "<dummy>"
        cmd_new = DefinitionCreator(root_word)
        expected_word = Word(root_word=root_word, metadata=self.a_metadata, derived_forms=DerivedForms(),
                             definitions=[a_definition()])

        mock_definition_reader.return_value = expected_word.definitions[0]
        mock_metadata.return_value = expected_word.metadata
        mock_derived_forms.return_value = expected_word.derived_forms

        with patch.object(ConsolePrinter, "output_message"):
            word = cmd_new._read_details()

        self.assertEqual(word.metadata, expected_word.metadata)
        self.assertEqual(word.definitions, expected_word.definitions)
        self.assertEqual(word.derived_forms, expected_word.derived_forms)


if __name__ == '__main__':
    unittest.main()
