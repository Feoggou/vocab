import unittest
from unittest.mock import patch

from vocabulary.app.word.updater import DefinitionUpdater
from vocabulary.domain.word import Word


class DefinitionUpdaterTest(unittest.TestCase):

    def setUp(self):
        self.my_definition = "<my_definition>"

        self.patcher_create_definition = patch.object(DefinitionUpdater, "_create_entry", autospec=True)
        self.mock_create_definition = self.patcher_create_definition.start()
        self.mock_create_definition.return_value = self.my_definition

        self.patcher_save_word = patch.object(DefinitionUpdater, "_save_word", autospec=True)
        self.mock_save_word = self.patcher_save_word.start()

        self.word_name = "myword"
        self.add_cmd = DefinitionUpdater(self.word_name)

    def tearDown(self):
        self.patcher_create_definition.stop()
        self.patcher_save_word.stop()

    # TODO: - add when none exited
    #       - add when an item existed (does not replace)
    #       - check result = correct word
    #       - test against idioms

    def test_addDefinition_createsDefinition_loadsJsonFile_updatesJson_andSavesFile(self):
        old_word = Word(self.word_name)
        new_word = Word(self.word_name, definitions=[self.my_definition])

        with patch.object(DefinitionUpdater, "_load_word", autospec=True) as mock_load:
            mock_load.return_value = old_word

            self.add_cmd.execute()

        self.mock_create_definition.assert_called_once()
        mock_load.assert_called_once()
        self.mock_save_word.assert_called_once_with(self.add_cmd, new_word)

    def test_addDefinition_whenOneItemExisted(self):
        old_word = Word(self.word_name, definitions=["dummy_definition"])
        new_word = Word(self.word_name, definitions=["dummy_definition", self.my_definition])

        with patch.object(DefinitionUpdater, "_load_word", autospec=True) as mock_load:
            mock_load.return_value = old_word

            self.add_cmd.execute()

        self.mock_save_word.assert_called_once_with(self.add_cmd, new_word)

    def test_addDefinition_hadNoDefinition_returns_wordDataObject(self):
        old_word = Word(self.word_name)
        new_word = Word(self.word_name, definitions=[self.my_definition])

        with patch.object(DefinitionUpdater, "_load_word", autospec=True) as mock_load:
            mock_load.return_value = old_word

            result = self.add_cmd.execute()

        self.assertEqual(result, new_word, msg="\n\nActual={}\nExpected={}\n".format(result, new_word))


if __name__ == "__main__":
    unittest.main()
