import unittest
from unittest.mock import patch

from vocabulary.app.operations.create import WordCreator
from vocabulary.domain.word import Word

from vocabulary.domain.entries.metadata import Metadata, MetadataReader
from vocabulary.domain.entries.derived_forms import DerivedForms, DerivedFormsReader

from tests.deep_copy_mock import DeepCopyMock


class WordCreatorTest(unittest.TestCase):
    def setUp(self):
        self.a_metadata = Metadata(origin='x', frequency=-1)

    def test_execute_readsDetails_savesJson_returnsJson(self):
        word_name = "<dummy_word>"
        newCmd = WordCreator(word_name)

        expected_word = Word(root_word=word_name, definitions=["definition"], idioms=["idiom"])

        with patch.object(WordCreator, "_read_details", autospec=True) as mock_read:
            mock_read.return_value = expected_word

            with patch.object(WordCreator, "_save_json", new_callable=DeepCopyMock) as mock_save:
                result = newCmd.execute()

        mock_read.assert_called_once()
        mock_save.assert_called_once_with(expected_word)

        self.assertEqual(result, expected_word)

    def test_readDetails_createsWordData_withRootWord(self):
        expected_root_word = "<dummy>"
        newCmd = WordCreator(expected_root_word)

        with patch.object(WordCreator, "_read_specific_details"):
            with patch.object(MetadataReader, "read"):
                with patch.object(DerivedFormsReader, "read"):
                    word = newCmd._read_details()

        self.assertEqual(word.word_name, expected_root_word)


if __name__ == '__main__':
    unittest.main()
