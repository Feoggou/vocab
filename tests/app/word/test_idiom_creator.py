import unittest
from unittest.mock import patch

from tests.utils import an_idiom
from vocabulary.app.operations.create import IdiomCreator
from vocabulary.domain.word import Word

from vocabulary.domain.entries.metadata import Metadata, MetadataReader
from vocabulary.domain.entries.derived_forms import DerivedForms, DerivedFormsReader
from vocabulary.domain.entries.idiom import Idiom, IdiomReader

from vocabulary.utils.utilities import ConsolePrinter


class IdiomCreatorTest(unittest.TestCase):
    def setUp(self):
        self.a_metadata = Metadata(origin='an_origin', frequency=-1)

    @patch.object(IdiomReader, "read")
    @patch.object(MetadataReader, "read")
    @patch.object(DerivedFormsReader, "read")
    def test_readDetails_calls_wordReaderFor_allEntries_and_returnsTheObjects(self,
                                                                              mock_derived_forms,
                                                                              mock_metadata,
                                                                              mock_idiom_reader):
        root_word = "<dummy"
        cmd_new = IdiomCreator(root_word)

        expected_word = Word(root_word=root_word, metadata=self.a_metadata, derived_forms=DerivedForms(),
                             idioms=[an_idiom(expression="dummy")])

        mock_idiom_reader.return_value = expected_word.idioms[0]
        mock_metadata.return_value = expected_word.metadata
        mock_derived_forms.return_value = expected_word.derived_forms

        with patch.object(ConsolePrinter, "output_message"):
            word = cmd_new._read_details()

        self.assertEqual(word.metadata, expected_word.metadata)
        self.assertEqual(word.derived_forms, expected_word.derived_forms)
        self.assertEqual(word.idioms, expected_word.idioms)


if __name__ == '__main__':
    unittest.main()
