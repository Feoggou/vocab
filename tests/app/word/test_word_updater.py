import unittest
from unittest.mock import patch

from vocabulary.domain.word import Word
from vocabulary.app.word.updater import DefinitionUpdater

from vocabulary.domain.entries.definition import DefinitionReader
from vocabulary.utils.utilities import ConsolePrinter


class WordUpdaterTest(unittest.TestCase):
    def test_createEntry_calls_structureReader(self):
        updater = DefinitionUpdater("root_word")
        expected_read = "<dummy>"

        with patch.object(ConsolePrinter, "output_message"):
            with patch.object(DefinitionReader, "read") as mock_read:
                mock_read.return_value = expected_read

                result = updater._create_entry()

        mock_read.assert_called_once()
        self.assertEqual(result, expected_read)

    def test_loadWordData_calls_wordLoader(self):
        word_name = "myword"
        updater = DefinitionUpdater(word_name)
        word = Word(word_name)

        with patch.object(Word, "load") as mock_load:
            mock_load.return_value = word

            result = updater._load_word()

        mock_load.assert_called_once_with(word_name)
        self.assertEqual(result, word)

    def test_saveWordData_calls_wordSaver(self):
        word_name = "myword"
        updater = DefinitionUpdater(word_name)
        word = Word(word_name)

        with patch.object(Word, "save") as mock_save:
            updater._save_word(word)

        mock_save.assert_called_once_with()


if __name__ == "__main__":
    unittest.main()
