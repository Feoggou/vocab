import unittest
from unittest.mock import patch

from tests.utils import a_definition, an_idiom
from vocabulary.app.word.matcher import DefinitionContentMatcher
from vocabulary.domain.word import Word

from vocabulary.app.search.as_expression import SearchAsExpression, SearchMethod
from vocabulary.app.search.where import SearchNowhere, SearchInExplanations


def not_colored_text(text: str):
    return text


class WordMatcherTest(unittest.TestCase):
    def test_whenHaveDefinitions_returnMatchingDefinitions(self):
        definitions = [
            a_definition(explanations=["I will just eat my breakfast"]),
            a_definition(explanations=["this will be ignored"]),
            a_definition(explanations=["I will eat you!"]),
            a_definition(explanations=["Ignore me!"]),
            a_definition(explanations=["I have been eaten alive."]),
            a_definition(explanations=["have you ignored me?"]),
        ]

        found_word = Word("<root>", definitions=definitions)

        with patch.object(SearchMethod, "_colored", new=not_colored_text):
            matcher = DefinitionContentMatcher(found_word, SearchAsExpression("eat", SearchInExplanations()))
            matches = matcher.find_matches()

        self.assertEqual(set(matches), {
            "I will just eat my breakfast",
            "I will eat you!"
        })

    def test_whenHaveIdioms_returnMatchingIdioms(self):
        idioms = [
            an_idiom(expression="dummy", explanations=["I will just eat my breakfast"]),
            an_idiom(expression="dummy", explanations=["this will be ignored"]),
            an_idiom(expression="dummy", explanations=["I will eat you!"]),
            an_idiom(expression="dummy", explanations=["Ignore me!"]),
            an_idiom(expression="dummy", explanations=["I have been eaten alive."]),
            an_idiom(expression="dummy", explanations=["have you ignored me?"]),
        ]

        found_word = Word("<root>", idioms=idioms)

        with patch.object(DefinitionContentMatcher, "_find_matches_in_idiom_expression") as mock_idiom_expr:
            mock_idiom_expr.return_value = []
            with patch.object(SearchMethod, "_colored", new=not_colored_text):
                matcher = DefinitionContentMatcher(found_word, SearchAsExpression("eat", SearchInExplanations()))
                matches = matcher.find_matches()

        self.assertEqual(set(matches), {
            "I will just eat my breakfast",
            "I will eat you!"
        })

    def test_whenWordIsBare_returnsEmptyList(self):
        found_word = Word("<root_word>")

        matcher = DefinitionContentMatcher(found_word, SearchAsExpression("eat", SearchNowhere()))
        matches = matcher.find_matches()

        self.assertEqual(matches, [])


if __name__ == "__main__":
    unittest.main()
