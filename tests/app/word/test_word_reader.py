import unittest
from unittest.mock import patch

from vocabulary.domain.entries.reader.item import ItemReader
from vocabulary.domain.entries.reader.frequency import FrequencyReader

from vocabulary.domain.entries.metadata import Metadata, MetadataReader
from vocabulary.utils.utilities import ConsolePrinter


class WordReaderTest(unittest.TestCase):

    def test_read_word(self):
        """
        Input: an empty metadata and a WordReader()
        Output: The metadata is filled in
        Expected behavior: must call the 'read' method on each attribute of metadata
        """

        expected_metadata = Metadata(origin="origin", frequency=4)

        with patch.object(ConsolePrinter, "output_message"):
            with patch.object(ItemReader, "read", autospec=True) as mock_item:
                mock_item.return_value = "origin"
                with patch.object(FrequencyReader, "read", autospec=True) as mock_frequency:
                    mock_frequency.return_value = 4

                    result = MetadataReader.read()

        self.assertEqual(result, expected_metadata,
                         "\n\nexpected={}\nactual={}\n\n".format(str(expected_metadata), str(result)))

    def test_whenInputIs_emptyLine_ItemReaderReturns_None(self):
        item_reader = ItemReader("")

        with patch('vocabulary.domain.entries.reader.item.input') as mock_input:
            mock_input.return_value = ""

            actual = item_reader.read()

        self.assertEqual(actual, None)


if __name__ == '__main__':
    unittest.main()
