import unittest
from unittest.mock import patch

from vocabulary.app.word.updater import IdiomUpdater
from vocabulary.domain.word import Word


class IdiomUpdaterTest(unittest.TestCase):

    def setUp(self):
        self.my_idiom = "<my_idiom>"

        self.patcher_create_idiom = patch.object(IdiomUpdater, "_create_entry", autospec=True)
        self.mock_create_idiom = self.patcher_create_idiom.start()
        self.mock_create_idiom.return_value = self.my_idiom

        self.patcher_save_word = patch.object(IdiomUpdater, "_save_word", autospec=True)
        self.mock_save_word = self.patcher_save_word.start()

        self.word_name = "myword"
        self.add_cmd = IdiomUpdater(self.word_name)

    def tearDown(self):
        self.patcher_create_idiom.stop()
        self.patcher_save_word.stop()

    def test_addDefinition_createsIdiom_loadsJsonFile_updatesJson_andSavesFile(self):
        old_word = Word(self.word_name)
        new_word = Word(self.word_name, idioms=[self.my_idiom])

        with patch.object(IdiomUpdater, "_load_word", autospec=True) as mock_load:
            mock_load.return_value = old_word

            self.add_cmd.execute()

        self.mock_create_idiom.assert_called_once()
        mock_load.assert_called_once()
        self.mock_save_word.assert_called_once_with(self.add_cmd, new_word)

    def test_addIdiom_whenOneItemExisted(self):
        old_word = Word(self.word_name, idioms=["dummy_idiom"])
        new_word = Word(self.word_name, idioms=["dummy_idiom", self.my_idiom])

        with patch.object(IdiomUpdater, "_load_word", autospec=True) as mock_load:
            mock_load.return_value = old_word

            self.add_cmd.execute()

        self.mock_save_word.assert_called_once_with(self.add_cmd, new_word)

    def test_addIdiom_hadNoDefinition_returns_wordDataObject(self):
        old_word = Word(self.word_name)
        new_word = Word(self.word_name, idioms=[self.my_idiom])

        with patch.object(IdiomUpdater, "_load_word", autospec=True) as mock_load:
            mock_load.return_value = old_word

            result = self.add_cmd.execute()

        self.assertEqual(result, new_word, msg="\n\nActual={}\nExpected={}\n".format(result, new_word))


if __name__ == "__main__":
    unittest.main()
