import unittest

from vocabulary.app.search.as_regex import SearchAsRegex
from vocabulary.app.search.where import SearchNowhere


class SearchAsRegexTest(unittest.TestCase):
    def test_simpleExpression_resultFound(self):
        search = SearchAsRegex("do.+mth", SearchNowhere())

        result = search.match("I want to undo smthing or perhaps.")

        self.assertEqual(result, "I want to un{}ing or perhaps.".format(search._colored("do smth")))

    def test_simpleExpression_differentCasing_resultFound(self):
        search = SearchAsRegex("do.+mth", SearchNowhere())

        result = search.match("I want to unDO smTHing or perhaps.")

        self.assertEqual(result, "I want to un{}ing or perhaps.".format(search._colored("DO smTH")))

    def test_simpleExpression_resultNotFound(self):
        search = SearchAsRegex("do.+", SearchNowhere())

        result = search.match("I want to.")

        self.assertEqual(result, None)

    def test_expressionIsFoundTwice(self):
        search = SearchAsRegex("do.{4,6}mth", SearchNowhere())

        result = search.match("I want todo oaamth or perhaps be doing smth else.")

        colored_match1 = search._colored("do oaamth")
        colored_match2 = search._colored("doing smth")

        self.assertEqual(result, "I want to{} or perhaps be {} else.".format(colored_match1, colored_match2))


if __name__ == "__main__":
    unittest.main()
