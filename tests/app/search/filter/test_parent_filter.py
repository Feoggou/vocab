import unittest

from tests.utils import a_definition
from vocabulary.domain.entries.definition import Definition
from vocabulary.app.search.filter.parent import ParentFilter


class ParentFilterTest(unittest.TestCase):
    def test_condition_simple_doesNotExist(self):
        definition = a_definition(parents=["a", "b"], explanations=["dummy"])

        have = ParentFilter("c").condition(definition)

        self.assertEqual(have, False)

    def test_condition_simple_exists(self):
        definition = a_definition(parents=["a", "b", "c"], explanations=["dummy"])

        have = ParentFilter("b").condition(definition)

        self.assertEqual(have, True)

    def test_condition_hierarchyItem_doesNotExist(self):
        definition = a_definition(parents=["a1/a2/a3", "b1/b2/b3"], explanations=["dummy"])

        have = ParentFilter("THIS").condition(definition)

        self.assertEqual(have, False)

    def test_condition_hierarchyItemExists_atTheBeginning(self):
        definition = a_definition(parents=["a1/a2/a3", "THIS/b2/b3"], explanations=["dummy"])

        have = ParentFilter("THIS").condition(definition)

        self.assertEqual(have, True)

    def test_condition_hierarchyItemExists_inTheMiddle(self):
        definition = a_definition(parents=["b1/THIS/b3", "c1/c2/c3"], explanations=["dummy"])

        have = ParentFilter("THIS").condition(definition)

        self.assertEqual(have, True)

    def test_condition_hierarchyItemExists_atTheEnd(self):
        definition = a_definition(parents=["c1/c2/THIS", "d1/d2/d3"], explanations=["dummy"])

        have = ParentFilter("THIS").condition(definition)

        self.assertEqual(have, True)

    def test_condition_multipleHierarchyItems_doesNotExist(self):
        definition = a_definition(parents=["a1/THIS/a3", "IS/b2/b3"], explanations=["dummy"])

        have = ParentFilter("THIS/IS").condition(definition)

        self.assertEqual(have, False)

    def test_condition_multipleHierarchyItemsExists_atTheBeginning(self):
        definition = a_definition(parents=["THIS/IS/a2/a3", "THIS/b2/b3"], explanations=["dummy"])

        have = ParentFilter("THIS/IS").condition(definition)

        self.assertEqual(have, True)

    def test_condition_multipleHierarchyItemsExists_inTheMiddle(self):
        definition = a_definition(parents=["b1/THIS/IS/b3", "c1/c2/c3"], explanations=["dummy"])

        have = ParentFilter("THIS/IS").condition(definition)

        self.assertEqual(have, True)

    def test_condition_multipleHierarchyItemsExists_atTheEnd(self):
        definition = a_definition(parents=["c1/c2/THIS/IS", "d1/d2/d3"], explanations=["dummy"])

        have = ParentFilter("THIS/IS").condition(definition)

        self.assertEqual(have, True)


if __name__ == "__main__":
    unittest.main()
