import unittest
from unittest.mock import patch, call

from tests.utils import a_definition, an_idiom
from vocabulary.domain.entries.definition import Definition
from vocabulary.domain.entries.idiom import Idiom
from vocabulary.domain.word import Word

from vocabulary.app.search.filter.definition import DefinitionFilter


class DefinitionFilterTest(unittest.TestCase):
    def test_callsFilterEntries_forDefinitionsAndIdioms(self):
        definitions = [a_definition(gram_value="vb")]
        idioms = [an_idiom(expression="my expression")]

        word = Word("<dummy name>", definitions=definitions, idioms=idioms)
        filter = DefinitionFilter("<dummy value>", lambda x: True)

        with patch.object(DefinitionFilter, "_filter_entries") as mock_filter:
            filter.do(word)

        mock_filter.assert_has_calls([call(definitions), call(idioms)])

    def test_filterEntries_filtersOnEachElement_ofEntries(self):
        filter = DefinitionFilter("<dummy value>", lambda x: x % 2 == 0)

        entries = [0, 1, 2, 3, 4, 5]
        expected = [0, 2, 4]

        result = filter._filter_entries(entries)

        self.assertListEqual(result, expected)

    def test_whenEntries_isNone_filterEntriesReturnsNone(self):
        filter = DefinitionFilter("<dummy value>", lambda x: True)

        result = filter._filter_entries(entries=None)

        self.assertIsNone(result)

    def test_definitionFilter_whenDefinitions_isNotNone_returnsWord(self):
        definitions = [a_definition(explanations=["explanation1"])]
        word = Word("<dummy>", definitions=definitions)

        filter = DefinitionFilter("4", lambda x: True)
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, word)

    def test_definitionFilter_whenDefinitionsAndIdioms_areNone_returnsWord(self):
        word = Word("<dummy>", definitions=None)

        filter = DefinitionFilter("4", lambda x: True)
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, word)

    def test_definitionFilter_whenIdioms_isNotNone(self):
        idioms = [
            an_idiom(expression="dummy", explanations=["explanation1"]),
            an_idiom(expression="dummy", explanations=["explanation2"])
        ]
        word = Word("<dummy>", idioms=idioms)

        expected_word = Word("<dummy>", idioms=[an_idiom(expression="dummy", explanations=["explanation2"])])

        # for the purpose of testing, any condition would do
        condition = lambda x: "explanation2" in x.explanations
        filter = DefinitionFilter("4", condition)
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_word)

    def test_definitionFilter_whenDefinitionsAndIdioms_areNotNone(self):
        definitions = [a_definition(gram_value="vt"), a_definition(gram_value="noun")]
        idioms = [an_idiom(expression="dummy", gram_value="noun"), an_idiom(expression="dummy", gram_value="vi")]
        word = Word("<dummy>", definitions=definitions, idioms=idioms)

        expected_word = Word("<dummy>", definitions=[a_definition(gram_value="noun")],
                             idioms=[an_idiom(expression="dummy", gram_value="noun")])

        filter = DefinitionFilter("4", lambda x: x.gram_value == "noun")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_word)


if __name__ == "__main__":
    unittest.main()
