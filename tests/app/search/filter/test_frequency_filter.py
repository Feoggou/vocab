import unittest

from tests.utils import a_definition
from vocabulary.app.search.filter.filter import *

from vocabulary.domain.entries.definition import Definition
from vocabulary.domain.entries.metadata import Metadata

from vocabulary.app.search.filter.frequency import FrequencyFilter


class FrequencyFilterTest(unittest.TestCase):
    def test_frequencyFilter_seekUknown_haveUnknown(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency=-1))

        filter = FrequencyFilter("-1")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, word)

    def test_frequencyFilter_seekUknown_haveUseless(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="0"))
        expected_word = Word("<dummy>", definitions=[], metadata=self.a_metadata(frequency="0"))

        filter = FrequencyFilter("-1")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_word)

    def test_frequencyFilter_seekUknown_haveGreaterThanZero(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="3"))
        expected_word = Word("<dummy>", definitions=[], metadata=self.a_metadata(frequency="3"))

        filter = FrequencyFilter("-1")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_word)

    def test_frequencyFilter_seekUseless_haveUnknown(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="-1"))
        expected_filtered_word = Word("<dummy>", definitions=[], metadata=self.a_metadata(frequency="-1"))

        filter = FrequencyFilter("0")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_filtered_word)

    def test_frequencyFilter_seekUseless_haveUseless(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="0"))

        filter = FrequencyFilter("0")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, word)

    def test_frequencyFilter_seekUseless_haveGreaterThanZero(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="3"))
        expected_word = Word("<dummy>", definitions=[], metadata=self.a_metadata(frequency="3"))

        filter = FrequencyFilter("0")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_word)

    def test_frequencyFilter_seekUseful_haveUnknown(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="-1"))
        expected_filtered_word = Word("<dummy>", definitions=[], metadata=self.a_metadata(frequency="-1"))

        filter = FrequencyFilter("2")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_filtered_word)

    def test_frequencyFilter_seekUsefull_haveSmaller(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="1"))
        expected_filtered_word = Word("<dummy>", definitions=[], metadata=self.a_metadata(frequency="1"))

        filter = FrequencyFilter("2")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_filtered_word)

    def test_frequencyFilter_seekUsefull_haveEqual(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="2"))

        filter = FrequencyFilter("2")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, word)

    def test_frequencyFilter_seekUsefull_haveGreater(self):
        definitions = [a_definition(explanations=["explanation1"]), a_definition(explanations=["explanation2"])]

        word = Word("<dummy>", definitions=definitions, metadata=self.a_metadata(frequency="4"))

        filter = FrequencyFilter("2")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, word)

    @staticmethod
    def a_metadata(frequency):
        return Metadata(origin='an_origin', frequency=frequency)


if __name__ == "__main__":
    unittest.main()
