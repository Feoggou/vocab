import unittest

from tests.utils import a_definition
from vocabulary.domain.word import Word

from vocabulary.app.search.filter.subject import SubjectFilter


class SubjectFilterTest(unittest.TestCase):
    def test_subjectFilter(self):
        definitions = [
            a_definition(subjects=["a", "b"], explanations=["explanation1"]),
            a_definition(subjects=["b", "c"], explanations=["explanation2"]),
            a_definition(subjects=["c", "d"], explanations=["explanation3"]),
        ]
        filtered_definitions = [
            a_definition(subjects=["b", "c"], explanations=["explanation2"]),
            a_definition(subjects=["c", "d"], explanations=["explanation3"]),
        ]
        word = Word("myword", definitions=definitions)
        expected_filtered = Word("myword", definitions=filtered_definitions)

        filter = SubjectFilter("c")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_filtered)


if __name__ == "__main__":
    unittest.main()
