import unittest

from tests.utils import a_definition
from vocabulary.domain.entries.definition import Definition
from vocabulary.domain.word import Word

from vocabulary.app.search.filter.usefulness import UsefulnessFilter


class UsefulnessFilterTest(unittest.TestCase):
    def test_usefulnessFilter_seekUknown(self):
        entries = [
            a_definition(usefulness="-1", explanations=["explanation1"]),
            a_definition(usefulness="0", explanations=["explanation2"]),
            a_definition(usefulness="1", explanations=["explanation3"]),
        ]
        filtered_entries = [
            a_definition(usefulness="-1", explanations=["explanation1"]),
        ]
        word = Word("myword", definitions=entries)
        expected_filtered = Word("myword", definitions=filtered_entries)

        filter = UsefulnessFilter("-1")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_filtered)

    def test_usefulnessFilter_seekUseless(self):
        entries = [
            a_definition(usefulness="-1", explanations=["explanation1"]),
            a_definition(usefulness="0", explanations=["explanation2"]),
            a_definition(usefulness="1", explanations=["explanation3"]),
        ]
        filtered_entries = [
            a_definition(usefulness="0", explanations=["explanation2"]),
        ]
        word = Word("myword", definitions=entries)
        expected_filtered = Word("myword", definitions=filtered_entries)

        filter = UsefulnessFilter("0")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_filtered)

    def test_usefulnessFilter_seekUseful(self):
        entries = [
            a_definition(usefulness="-1", explanations=["explanation1"]),
            a_definition(usefulness="2", explanations=["explanation2"]),
            a_definition(usefulness="1", explanations=["explanation3"]),
        ]
        filtered_entries = [
            a_definition(usefulness="2", explanations=["explanation2"]),
            a_definition(usefulness="1", explanations=["explanation3"]),
        ]
        word = Word("myword", definitions=entries)
        expected_filtered = Word("myword", definitions=filtered_entries)

        filter = UsefulnessFilter("1")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_filtered)


if __name__ == "__main__":
    unittest.main()
