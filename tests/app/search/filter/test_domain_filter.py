import unittest

from tests.utils import a_definition
from vocabulary.domain.word import Word

from vocabulary.app.search.filter.domain import DomainFilter


class DomainFilterTest(unittest.TestCase):
    def test_domainFilter(self):
        entries = [
            a_definition(domains=["a", "b"], explanations=["explanation1"]),
            a_definition(domains=["b", "c"], explanations=["explanation2"]),
            a_definition(domains=["c", "d"], explanations=["explanation3"]),
        ]
        filtered_entries = [
            a_definition(domains=["b", "c"], explanations=["explanation2"]),
            a_definition(domains=["c", "d"], explanations=["explanation3"]),
        ]
        word = Word("myword", definitions=entries)
        expected_filtered = Word("myword", definitions=filtered_entries)

        filter = DomainFilter("c")
        filtered_word = filter.do(word)

        self.assertEqual(filtered_word, expected_filtered)


if __name__ == "__main__":
    unittest.main()
