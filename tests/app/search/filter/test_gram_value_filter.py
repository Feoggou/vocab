import unittest
from unittest.mock import patch

from vocabulary.app.search.filter.gram_value import GramValueFilter
from vocabulary.app.search.filter.definitions_matcher import DefinitionsMatcher

from vocabulary.domain.entries.derived_forms import DerivedForms
from vocabulary.domain.word import Word


class GramValueFilterTest(unittest.TestCase):
    def test_gramValueFilter(self):
        input_word = Word("<name1>", definitions=["definitions_in"], idioms=["idioms_in"],
                          derived_forms=DerivedForms(noun="yes"))

        expected_word = Word("<name1>", definitions=["output_definitions1", "output_definitions2"],
                             idioms=["output_idiom1"], derived_forms=DerivedForms(noun="yes"))

        gram_filter = GramValueFilter("<dummy>")

        with patch.object(DefinitionsMatcher, "find_matches") as mock_find_matches:
            mock_find_matches.side_effect = [["output_definitions1", "output_definitions2"], ["output_idiom1"]]

            result = gram_filter.do(input_word)

        self.assertEqual(result, expected_word)


if __name__ == "__main__":
    unittest.main()
