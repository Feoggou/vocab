import unittest

from tests.utils import a_definition
from vocabulary.app.search.filter.definitions_matcher import DefinitionsMatcher

from vocabulary.domain.entries.derived_forms import DerivedForms


class DefinitionsMatcherTest(unittest.TestCase):
    def setUp(self):
        self.definition_adj = a_definition(gram_value="adj", explanations=["explanation1"])
        self.definition_adv = a_definition(gram_value="adv", explanations=["explanation2"])
        self.definition_noun = a_definition(gram_value="noun", explanations=["explanation3"])
        self.definition_verb = a_definition(gram_value="vb", explanations=["explanation4"])
        self.definition_vt = a_definition(gram_value="vt", explanations=["explanation5"])
        self.definition_vi = a_definition(gram_value="vi", explanations=["explanation6"])

        self.full_definitions = [
            self.definition_adj,
            self.definition_adv,
            self.definition_noun,
            self.definition_verb,
            self.definition_vt,
            self.definition_vi
        ]

    def test_filter_noun(self):
        expected_definitions = [self.definition_noun]
        matcher = DefinitionsMatcher(self.full_definitions)

        result = matcher.find_matches("noun")

        self.assertListEqual(result, expected_definitions)

    def test_filter_verb(self):
        expected_definitions = [self.definition_verb, self.definition_vt, self.definition_vi]
        matcher = DefinitionsMatcher(self.full_definitions)

        result = matcher.find_matches("vb")

        self.assertListEqual(result, expected_definitions)

    def test_filter_vt(self):
        expected_definitions = [self.definition_verb, self.definition_vt]
        matcher = DefinitionsMatcher(self.full_definitions)

        result = matcher.find_matches("vt")

        self.assertListEqual(result, expected_definitions)

    def test_filter_vi(self):
        expected_definitions = [self.definition_verb, self.definition_vi]
        matcher = DefinitionsMatcher(self.full_definitions)

        result = matcher.find_matches("vi")

        self.assertListEqual(result, expected_definitions)


class DefinitionMatcherWithDerivedFormsTest(unittest.TestCase):
    @staticmethod
    def build_definitions(gram_values: list):
        definitions = [a_definition(gram_value=value) for value in gram_values]

        return definitions

    def test_filter_noun_noMatchingGramValue(self):
        expected_definitions = self.build_definitions(["adj", "adv"])
        matcher = DefinitionsMatcher(self.build_definitions(["adj", "adv"]))

        result = matcher.find_matches("noun", DerivedForms(noun="yes"))

        self.assertListEqual(result, expected_definitions)

    def test_filter_noun_withMatchingGramValue(self):
        expected_definitions = self.build_definitions(["noun"])
        matcher = DefinitionsMatcher(self.build_definitions(["adj", "adv", "noun"]))

        result = matcher.find_matches("noun", DerivedForms(noun="yes"))

        self.assertListEqual(result, expected_definitions)

    def test_whenWeFindNoMatchAndDerivedFormsIsNone_definitionBecomesEmpty(self):
        expected_definitions = self.build_definitions([])
        matcher = DefinitionsMatcher(self.build_definitions(["noun", "adj", "vt"]))

        result = matcher.find_matches("adv", forms=None)

        self.assertListEqual(result, expected_definitions)

    def test_whenWeFindNoMatchAndDerivedFormsHasItems_definitionBecomesEmpty(self):
        expected_definitions = self.build_definitions([])
        matcher = DefinitionsMatcher(self.build_definitions(["noun", "adj", "vt"]))

        result = matcher.find_matches("adv", DerivedForms(noun="yes"))

        self.assertListEqual(result, expected_definitions)

if __name__ == "__main__":
    unittest.main()
