import unittest
from unittest.mock import patch, call

from vocabulary.cli.commands.search import Search
from vocabulary.app.word.matcher import DefinitionContentMatcher
from vocabulary.domain.word import Word

from vocabulary.app.search.result import FoundInWord


class SearchTest(unittest.TestCase):
    def test_searchWord_extractsWordForms_parseFiles_returnsResults(self):
        search_cmd = Search("<dummy_word>", filters=[])
        expected_results = ["r1", "r2", "r3"]

        with patch.object(search_cmd, "search_files", autospec=True) as mock_search_files:
            mock_search_files.return_value = expected_results

            results = search_cmd.execute()

        mock_search_files.assert_called_once()
        self.assertEqual(results, expected_results)

    # TODO: consider removal
    def test_searchFiles_getsWords_callsMatcher(self):
        cmd_search = Search("<dummy_word>", filters=[])

        word1 = Word("word1")
        word2 = Word("word2")
        word3 = Word("word3")
        word4 = Word("word4")

        with patch("vocabulary.utils.utilities.get_vocab_words", autospec=True) as mock_enum:
            mock_enum.return_value = ["file1", "file2", "file3", "file4"]
            with patch.object(Word, "load") as mock_load:
                mock_load.side_effect = [word1, word2, word3, word4]
                with patch.object(Search, "_search_in_word") as mock_search_in_word:
                    # or "match_explanation{i}"?
                    mock_search_in_word.side_effect = [
                        None, "match_definition1", "match_definition2", "match_definition3", None
                    ]

                    result = cmd_search.search_files()

        mock_enum.assert_called_once()
        mock_load.assert_has_calls([call("file1"), call("file2"), call("file3"), call("file4")])
        mock_search_in_word.assert_has_calls([call(word1), call(word2), call(word3), call(word4)])
        self.assertEqual(result.get_items(), ["match_definition1", "match_definition2", "match_definition3"])

    def test_searchFiles_getsWords_searchesInEachWord(self):
        cmd_search = Search("<dummy_word>", filters=[])

        word1 = Word("word1")
        word2 = Word("word2")
        word3 = Word("word3")
        word4 = Word("word4")

        with patch("vocabulary.utils.utilities.get_vocab_words", autospec=True) as mock_enum:
            mock_enum.return_value = ["file1", "file2", "file3", "file4"]
            with patch.object(Word, "load") as mock_load:
                mock_load.side_effect = [word1, word2, word3, word4]
                with patch.object(cmd_search, "_search_in_word", autospec=True) as mock_search:
                    cmd_search.search_files()

        mock_enum.assert_called_once()
        mock_load.assert_has_calls([call("file1"), call("file2"), call("file3"), call("file4")])
        mock_search.assert_has_calls([call(word1), call(word2), call(word3), call(word4)])

    def test_searchInWord_returns_resultInWord(self):
        cmd_search = Search("<dummy>", filters=[])
        found_word = Word("word1")

        matching_explanations = ["dummy_explanation1", "dummy_explanation2"]
        expected = FoundInWord(found_word.word_name, matching_explanations)

        with patch.object(DefinitionContentMatcher, "find_matches") as mock_matches:
            mock_matches.return_value = matching_explanations

            result = cmd_search._search_in_word(found_word)

        self.assertEqual(result, expected)

    def test_searchInWord_useFilter_matches(self):
        cmd_search = Search("<dummy>", filters=[])
        found_word = Word("word1", definitions=["definition_matched", "definition_unmatched"])
        filtered_word = Word("word1", definitions=["definition_matched"])

        with patch.object(Search, "_find_matches") as mock_matches:
            mock_matches.return_value = []
            with patch.object(Search, "_apply_filters") as mock_apply_filters:
                mock_apply_filters.return_value = filtered_word

                cmd_search._search_in_word(found_word)

        mock_apply_filters.assert_called_once_with(found_word)
        mock_matches.assert_called_once_with(filtered_word)


if __name__ == "__main__":
    unittest.main()
