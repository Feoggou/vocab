import unittest

from vocabulary.app.search.as_expression import SearchAsExpression
from vocabulary.app.search.where import SearchNowhere


# TODO: Where in code can I check for quoted text in "to search"? I need to put a test there.

class SearchAsExpressionTest(unittest.TestCase):
    def test_simpleExpression_resultFound(self):
        search = SearchAsExpression("do smth", SearchNowhere())

        result = search.match("I want to do smth or perhaps.")

        self.assertEqual(result, "I want to {} or perhaps.".format(search._colored("do smth")))

    def test_simpleExpression_differentCasing_resultFound(self):
        search = SearchAsExpression("do smth", SearchNowhere())

        result = search.match("I want to DO sMTh or perhaps.")

        self.assertEqual(result, "I want to {} or perhaps.".format(search._colored("do smth")))

    def test_simpleExpression_resultNotFound(self):
        search = SearchAsExpression("do smth", SearchNowhere())

        result = search.match("I want to.")

        self.assertEqual(result, None)

    def test_expressionIsNotInWordBoundary_resultFound(self):
        search = SearchAsExpression("do smth", SearchNowhere())

        result1 = search.match("I want todo smth or perhaps.")
        result2 = search.match("I want to do smthor perhaps.")

        self.assertEqual(result1, None)
        self.assertEqual(result2, None)

    def test_expressionIsFoundTwice(self):
        search = SearchAsExpression("do smth", SearchNowhere())

        result = search.match("I want to do smth or perhaps do smth else.")

        colored_match = search._colored("do smth")
        self.assertEqual(result, "I want to {} or perhaps {} else.".format(colored_match, colored_match))

    def test_expressionIsThenIsNotInWordBoundary_resultFound(self):
        search = SearchAsExpression("do smth", SearchNowhere())

        result = search.match("I want todo smth or perhaps to do smth.")

        self.assertEqual(result, "I want todo smth or perhaps to {}.".format(search._colored("do smth")))


if __name__ == "__main__":
    unittest.main()
