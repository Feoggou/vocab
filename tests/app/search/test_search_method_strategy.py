import unittest

from vocabulary.app.search.method_strategy import SearchMethodStrategy
from vocabulary.app.search.as_expression import SearchAsExpression
from vocabulary.app.search.as_regex import SearchAsRegex
from vocabulary.app.search.where import SearchNowhere


class SearchMethodStrategyTest(unittest.TestCase):
    def test_whenHaveSingleWord_pickSearchAsExpression(self):
        picker = SearchMethodStrategy(SearchNowhere())

        result = picker.pick_method_for("eat")

        self.assertIsInstance(result, SearchAsExpression)

    def test_whenHaveMultipleWords_pickSearchAsExpression(self):
        picker = SearchMethodStrategy(SearchNowhere())

        result = picker.pick_method_for("eat dust")

        self.assertIsInstance(result, SearchAsExpression)

    def test_whenHaveQuotedText_pickSearchAsExpression_andRemovesQuotes(self):
        picker = SearchMethodStrategy(SearchNowhere())

        result = picker.pick_method_for('"eat dust"')

        self.assertIsInstance(result, SearchAsExpression)
        self.assertEqual(result.what, 'eat dust')

    def test_whenHaveQuotedWord_pickSearchAsExpression_andRemovesQuotes(self):
        picker = SearchMethodStrategy(SearchNowhere())

        result = picker.pick_method_for('"eat"')

        self.assertIsInstance(result, SearchAsExpression)
        self.assertEqual(result.what, 'eat')

    def test_whenHaveSpecialChars_pickSearchAsRegex_(self):
        picker = SearchMethodStrategy(SearchNowhere())

        result = picker.pick_method_for(r"eat.+e?")

        self.assertIsInstance(result, SearchAsRegex)
        self.assertEqual(result.what, r"eat.+e?")

    def test_whenHaveSpecialCharsAndSpace_pickSearchAsRegex_(self):
        picker = SearchMethodStrategy(SearchNowhere())

        result = picker.pick_method_for(r"eat .+e?")

        self.assertIsInstance(result, SearchAsRegex)
        self.assertEqual(result.what, r"eat .+e?")

    def test_whenHaveQuotedRegex_pickSearchAsRegex_(self):
        picker = SearchMethodStrategy(SearchNowhere())

        result = picker.pick_method_for(r'"eat.+e?"')

        self.assertIsInstance(result, SearchAsRegex)
        self.assertEqual(result.what, r'eat.+e?')

    def test_whenHaveQuotedRegexWithSpace_pickSearchAsRegex_(self):
        picker = SearchMethodStrategy(SearchNowhere())

        result = picker.pick_method_for(r'"eat. +e?"')

        self.assertIsInstance(result, SearchAsRegex)
        self.assertEqual(result.what, r'eat. +e?')


if __name__ == "__main__":
    unittest.main()
