import unittest
from unittest import mock

from vocabulary.utils import config, utilities


class UtilitiesTest(unittest.TestCase):
    def setUp(self):
        self.patcher = mock.patch("os.listdir")

    def tearDown(self):
        self.patcher.stop()

    def test_getVocabWords_returnsWordsOrdered(self):
        mock_listdir = self.mockListDirWithItems(["myword2.word", "myword3.word", "myword1.word"])

        found_files = utilities.get_vocab_words()

        mock_listdir.assert_called_once_with(config.OUTPUT_JSON_DIR)
        self.assertEqual(found_files, ["myword1", "myword2", "myword3"])

    def test_getVocabWords_returnsWords_onlyForVocabFiles(self):
        mock_listdir = self.mockListDirWithItems(["a.word", "b.txt", ".files", "c.word"])

        found_files = utilities.get_vocab_words()

        mock_listdir.assert_called_once_with(config.OUTPUT_JSON_DIR)
        self.assertEqual(found_files, ["a", "c"])

    def mockListDirWithItems(self, fs_items: list):
        mock_listdir = self.patcher.start()
        mock_listdir.return_value = fs_items
        return mock_listdir


if __name__ == "__main__":
    unittest.main()
