#!/usr/bin/env bash

export PYTHONPATH=$(dirname "$(pwd)")

for x in $(find . -type d); do
    have=$(expr match "$x" '.*__pycache__.*')
    if [ $? != 0 ]; then
        nose2 -s $x
    fi
done

