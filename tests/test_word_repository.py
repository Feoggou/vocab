import unittest
from unittest.mock import patch, call, Mock

from vocabulary.app.word.io import WordIO
from vocabulary.app.word.word_repository import WordRepository
from vocabulary.domain.word import Word
from vocabulary.app.word.iterator import WordIterator


class WordRepositoryTest(unittest.TestCase):
    def test_rename_parents(self):
        repository = WordRepository(cache=Mock())
        what = "parents"

        words_in = [Word("word1_in"), Word("word2_in"), Word("word3_in")]
        words_out = [Word("word1_out"), Word("word2_out"), Word("word3_out")]
        expected_calls = [call(word) for word in words_out]

        with patch.object(WordIterator, "__next__") as mock_next:
            mock_next.return_value = words_in
            with patch.object(Word, "rename_tags") as mock_rename:
                mock_rename.side_effect = words_out
                with patch.object(WordIO, "save") as mock_save:

                    repository.rename_tag(what, "old_name", "new_name")

        mock_rename.assert_called_with(what, "old_name", "new_name")
        mock_save.assert_has_calls(expected_calls)

    def test_rename_parents_ifNothingChanged_doNotSave(self):
        repository = WordRepository(cache=Mock())
        what = "parents"

        words_in = [Word("word")]
        words_out = [Word("word")]

        with patch.object(WordIterator, "__next__") as mock_next:
            mock_next.return_value = words_in
            with patch.object(Word, "rename_tags") as mock_rename:
                mock_rename.side_effect = words_out
                with patch.object(WordIO, "save") as mock_save:
                    repository.rename_tag(what, "old_name", "new_name")

        mock_rename.assert_called_with(what, "old_name", "new_name")
        mock_save.assert_not_called()

    def test_can_find_that_has_a_word(self):
        cache = Mock()
        cache.all = ["word1", "word2", "the_word", "word3"]
        repo = WordRepository(cache)

        have = repo.have_word("the_word")

        self.assertTrue(have)

    def test_can_find_that_it_does_not_a_word(self):
        cache = Mock()
        cache.all = ["word1", "word2", "word3", "word4"]
        repo = WordRepository(cache)

        have = repo.have_word("nonexisting-word")

        self.assertFalse(have)


if __name__ == '__main__':
    unittest.main()
