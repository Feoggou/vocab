import copy
from unittest import mock


class DeepCopyMock(mock.MagicMock):
    def __call__(self, *args, **kwargs):
        args = copy.deepcopy(args)
        kwargs = copy.deepcopy(kwargs)
        return super(DeepCopyMock, self).__call__(*args, **kwargs)
