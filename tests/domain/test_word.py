from vocabulary.domain.word import Word, DefinitionList
from vocabulary.app.word.file import WordFile
from vocabulary.domain.entries.definition import Definition
from vocabulary.domain.entries.idiom import Idiom

import unittest
from unittest.mock import patch, call, Mock

from tests import utils


class WordTest(unittest.TestCase):
    def test_save(self):
        word_name = "myword"
        word = Word(word_name)

        json_object = "<dummy>"

        with patch.object(Word, "_to_json") as mock_to_json:
            mock_to_json.return_value = json_object
            with patch.object(Word, "_save_json") as mock_save_json:
                word.save()

        mock_to_json.assert_called_once()
        mock_save_json.assert_called_once_with(json_object)

    @patch.object(Word, "_from_json")
    @patch.object(WordFile, "read")
    def test_loadWord(self, mock_read, mock_from_json):
        expected_word = Word("<root>")
        mock_read.return_value = {"json": "word"}
        mock_from_json.return_value = expected_word

        result = Word.load("<word-name>")

        self.assertIs(result, expected_word)


class DefinitionListTest(unittest.TestCase):
    def test_rename_tags(self):
        definition_list = DefinitionList([
            utils.a_definition("in_1"), utils.a_definition("in_2"), utils.a_definition("in_3")
        ])
        what = "parents"

        expected_definitions = ["out_definition1", "out_definition2", "out_definition3"]

        with patch.object(Definition, "rename_tags", autospec=True) as mock_rename:
            mock_rename.side_effect = expected_definitions

            result = definition_list.rename_tags(what, "old_name", "new_name")

        self.assertEqual(result, expected_definitions)

    def test_editor_whenDefinitions_isNone(self):
        definition_list = DefinitionList(None)
        what = "parents"

        result = definition_list.rename_tags(what, "old_name", "new_name")

        self.assertEqual(result, [])


class RenameWordTest(unittest.TestCase):
    def test_rename_tags(self):
        word = Word("word1", definitions="old_definitions", idioms="old_idioms")
        what = "parents"

        defs_mock = Mock(spec=Definition)
        idioms_mock = Mock(spec=Idiom)

        with patch("vocabulary.domain.word.DefinitionList") as mock_class:
            mock_class.side_effect = [defs_mock, idioms_mock]

            defs_mock.rename_tags.return_value = "new_definitions"
            idioms_mock.rename_tags.return_value = "new_idioms"

            word.rename_tags(what, "old_parent_name", "new_parent_name")

        mock_class.assert_has_calls([call("old_definitions"), call("old_idioms")])
        defs_mock.rename_tags.assert_called_once_with(what, "old_parent_name", "new_parent_name")
        idioms_mock.rename_tags.assert_called_once_with(what, "old_parent_name", "new_parent_name")

    def test_editor_usesACopyOfWord(self):
        word = Word("wordname")

        new_word = word.rename_tags("", "", "")

        self.assertIsNot(new_word, word)


if __name__ == "__main__":
    unittest.main()
