import unittest
from unittest.mock import patch

from vocabulary.domain.entries.reader.derived_forms_item_reader import DerivedFormsItemReader, ItemReaderWithSetOfPossibleValues


class DerivedFormsItemReaderTest(unittest.TestCase):
    patcher_output = None

    def setUp(self):
        self.reader = DerivedFormsItemReader("Have noun")

        self.patcher_output = patch.object(DerivedFormsItemReader, "output_valid")
        self.patcher_read = patch.object(ItemReaderWithSetOfPossibleValues, "read")

        self.mock_output = self.patcher_output.start()
        self.mock_read = self.patcher_read.start()

    def tearDown(self):
        self.patcher_output.stop()
        self.patcher_read.stop()

    def test_answerIs_yes(self):
        self.mock_read.return_value = "yes"

        result = self.reader.read()

        self.assertEqual(result, "yes")

    def test_whenAnswerIs_y_return_yes(self):
        self.mock_read.return_value = "y"

        result = self.reader.read()

        self.assertEqual(result, "yes")

    def test_answerIs_no(self):
        self.mock_read.return_value = "no"

        result = self.reader.read()

        self.assertEqual(result, "no")

    def test_whenAnswerIs_n_understand_no(self):
        self.mock_read.return_value = "n"

        result = self.reader.read()

        self.assertEqual(result, "no")

    def test_whenAnswerIsEmpty_understand_no(self):
        self.mock_read.return_value = None

        result = self.reader.read()

        self.assertEqual(result, "no")


if __name__ == '__main__':
    unittest.main()
