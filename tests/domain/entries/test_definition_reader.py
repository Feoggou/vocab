import unittest
from unittest.mock import patch

from vocabulary.domain.entries.definition import *
from tests import utils


class DefinitionReaderTest(unittest.TestCase):
    @patch.object(UsefulnessReader, "read")
    @patch.object(ItemReader, "read")
    @patch.object(StyleReader, "read")
    @patch.object(ObjectReader, "read")
    @patch.object(ParentReader, "read")
    @patch.object(SubjectReader, "read")
    @patch.object(DomainReader, "read")
    @patch.object(ListReader, "read")
    @patch.object(NotEmptyListReader, "read")
    @patch.object(GramValueReader, "read")
    def test_definition(self, mock_gram, mock_explanations, mock_examples, mock_domain,
                        mock_subject, mock_parent, mock_object, mock_style, mock_notes, mock_usefulness):

        expected_definition = Definition(gram_value="verb", explanations=["my explanation"], notes="my notes",
                                         usefulness=3,
                                         examples=["ex1", "ex2"], domains=["c1", "c2"], subjects=["subj1", "subj2"],
                                         objects=["obj1", "obj2"], parents=["p1"], styles=["s1", "s2", "s3"])

        mock_gram.return_value = expected_definition.gram_value
        mock_explanations.return_value = expected_definition.explanations
        mock_examples.return_value = expected_definition.examples
        mock_domain.return_value = expected_definition.domains
        mock_subject.return_value = expected_definition.subjects
        mock_parent.return_value = expected_definition.parents
        mock_style.return_value = expected_definition.styles
        mock_object.return_value = expected_definition.objects
        mock_notes.return_value = expected_definition.notes
        mock_usefulness.return_value = expected_definition.usefulness

        definition = DefinitionReader.read()

        self.assertEqual(definition, expected_definition)

    def test_rename_parents(self):
        in_definition = utils.a_definition(gram_value="verb", explanations=["my-explanation"],
                                           parents=["p1", "p2", "old", "p3"])
        what = "parents"

        expected_definition = utils.a_definition(gram_value="verb", explanations=["my-explanation"],
                                                 parents=["p1", "p2", "new", "p3"])
        result = in_definition.rename_tags(what, "old", "new")

        self.assertEqual(result, expected_definition)

    def test_rename_domains(self):
        in_definition = utils.a_definition(gram_value="verb", explanations=["my-explanation"],
                                           domains=["c1", "c2", "old", "c3"])
        what = "domains"

        expected_definition = utils.a_definition(gram_value="verb", explanations=["my-explanation"],
                                                 domains=["c1", "c2", "new", "c3"])
        result = in_definition.rename_tags(what, "old", "new")

        self.assertEqual(result, expected_definition)


if __name__ == '__main__':
    unittest.main()
