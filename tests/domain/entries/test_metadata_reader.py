import unittest
from unittest.mock import patch

from vocabulary.domain.entries.metadata import *


class MetadataReaderTest(unittest.TestCase):
    @patch.object(ItemReader, "read")
    @patch.object(FrequencyReader, "read")
    def test_can_read_metadata(self, frequency_read, origin_read):
        expected_metadata = self.a_metadata(frequency_read, origin_read)

        actual = MetadataReader.read()

        self.assertEqual(actual, expected_metadata)

    @staticmethod
    def a_metadata(frequency_read, origin_read):
        expected_metadata = Metadata(origin="an origin", frequency=3)
        origin_read.return_value = expected_metadata.origin
        frequency_read.return_value = expected_metadata.frequency
        return expected_metadata


if __name__ == '__main__':
    unittest.main()
