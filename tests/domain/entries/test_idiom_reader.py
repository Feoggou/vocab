import unittest
from unittest.mock import patch

from vocabulary.domain.entries.idiom import *
from tests import utils


class IdiomReaderTest(unittest.TestCase):
    @patch.object(NotEmptyItemReader, "read")
    @patch.object(DefinitionReader, "read")
    def test_definition(self, mock_definition, mock_idiom):
        definition = utils.a_definition(gram_value="vt")
        expression = "my expression"

        expected_idiom = Idiom.from_definition(expression=expression, definition=definition)

        mock_definition.return_value = definition
        mock_idiom.return_value = expression

        idiom = IdiomReader.read()

        self.assertEqual(idiom, expected_idiom)


if __name__ == '__main__':
    unittest.main()
