import unittest
from unittest.mock import patch, call

from vocabulary.domain.entries.reader.not_empty_item import NotEmptyItemReader
from vocabulary.domain.entries.reader.item import ItemReader


class NotEmptyItemReaderTest(unittest.TestCase):
    def setUp(self):
        self.reader = NotEmptyItemReader("dummy")

        self.patcher_read = patch.object(ItemReader, "read")
        self.patcher_output_valid = patch.object(NotEmptyItemReader, "output_valid")

        self.mock_read = self.patcher_read.start()
        self.mock_output_valid = self.patcher_output_valid.start()

    def tearDown(self):
        self.patcher_read.stop()
        self.patcher_output_valid.stop()

    def test_read_oneItem(self):
        self.mock_read.return_value = "item"

        result = self.reader.read()

        self.assertEqual(result, "item")
        self.mock_output_valid.assert_called_once()

    def test_read_empty_then_not_empty(self):
        self.mock_read.side_effect = [None, "item"]

        result = self.reader.read()

        self.assertEqual(result, "item")
        self.mock_output_valid.assert_has_calls([call(), call()])


if __name__ == '__main__':
    unittest.main()
