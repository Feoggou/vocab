import unittest

from vocabulary.domain.entries.reader.style import StyleReader
from vocabulary.app.tag.style_catalog import StyleCatalog


class StyleReaderTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        reader = StyleReader(msg="dummy")
        self.assertIsInstance(reader.get_catalog(), StyleCatalog)


if __name__ == '__main__':
    unittest.main()
