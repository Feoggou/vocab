import unittest

from vocabulary.domain.entries.reader.object import ObjectReader
from vocabulary.app.tag.object_catalog import ObjectCatalog


class ObjectReaderTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        reader = ObjectReader(msg="dummy")
        self.assertIsInstance(reader.get_catalog(), ObjectCatalog)


if __name__ == '__main__':
    unittest.main()
