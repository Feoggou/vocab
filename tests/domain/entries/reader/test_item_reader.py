import unittest
from unittest.mock import patch

from vocabulary.domain.entries.reader.item import ItemReader


class ItemReaderTest(unittest.TestCase):
    def setUp(self):
        self.reader = ItemReader("dummy")
        self.patcher_input = patch("builtins.input")

        self.mock_input = self.patcher_input.start()

    def tearDown(self):
        self.patcher_input.stop()

    def test_readText(self):
        self.mock_input.return_value = "an item"

        result = self.reader.read()

        self.assertEqual(result, "an item")

    def test_readNothing(self):
        self.mock_input.return_value = ""

        result = self.reader.read()

        self.assertEqual(result, None)


if __name__ == '__main__':
    unittest.main()
