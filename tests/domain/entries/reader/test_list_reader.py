import unittest
from unittest.mock import patch

from vocabulary.domain.entries.reader.list import ListReader
from vocabulary.domain.entries.reader.item import ItemReader


class ListReaderTest(unittest.TestCase):
    def setUp(self):
        self.reader = ListReader("dummy", ItemReader(msg="dummy"))
        self.patcher_read = patch.object(ItemReader, "read")

        self.mock_read = self.patcher_read.start()

    def tearDown(self):
        self.patcher_read.stop()

    def test_read_oneItem(self):
        self.mock_read.side_effect = ["item", None]

        result = self.reader.read()

        self.assertListEqual(result, ["item"])

    def test_read_twoItems(self):
        self.mock_read.side_effect = ["item1", "item2", None]

        result = self.reader.read()

        self.assertListEqual(result, ["item1", "item2"])

    def test_read_noItem(self):
        self.mock_read.return_value = None

        result = self.reader.read()

        self.assertListEqual(result, [])


if __name__ == '__main__':
    unittest.main()
