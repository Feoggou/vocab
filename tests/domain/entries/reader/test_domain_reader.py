import unittest

from vocabulary.domain.entries.reader.domain import DomainReader
from vocabulary.app.tag.domain_catalog import DomainCatalog


class DomainReaderTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        reader = DomainReader(msg="dummy")
        self.assertIsInstance(reader.get_catalog(), DomainCatalog)


if __name__ == '__main__':
    unittest.main()
