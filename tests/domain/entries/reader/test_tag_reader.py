import unittest
from unittest.mock import patch

from vocabulary.domain.entries.reader.line_list import LineListReader
from vocabulary.domain.entries.reader.tag import TagReader

from vocabulary.app.tag.catalog import Catalog


class TagReaderTest(unittest.TestCase):
    def test_read(self):
        reader = TagReader(msg="dummy", catalog=Catalog("dummy"))
        catalog_items = ["x1", "x2", "x3", "x4"]
        read_items = ["x1", "x3"]

        with patch.object(Catalog, "read") as mock_catalog_read:
            mock_catalog_read.return_value = catalog_items
            with patch.object(TagReader, "output_existing") as mock_output:
                with patch.object(LineListReader, "read") as mock_line_read:
                    mock_line_read.return_value = read_items
                    with patch.object(Catalog, "update_with") as mock_catalog_update:

                        result = reader.read()

        mock_line_read.assert_called_once()
        mock_output.assert_called_once_with(catalog_items)
        self.assertListEqual(result, read_items)
        mock_catalog_update.assert_called_once_with(read_items)


if __name__ == '__main__':
    unittest.main()
