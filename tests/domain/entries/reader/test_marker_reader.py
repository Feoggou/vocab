import unittest
from unittest.mock import patch, call

from vocabulary.domain.entries.reader.marker_reader import MarkerReader
from vocabulary.domain.entries.reader.item import ItemReader


class MarkerReaderTest(unittest.TestCase):
    def setUp(self):
        self.reader = MarkerReader(msg="dummy", valid_values={"dummy": "dummy"})

        self.patcher_output = patch.object(MarkerReader, "output_valid")
        self.patcher_is_valid = patch.object(MarkerReader, "is_valid")
        self.patcher_read = patch.object(ItemReader, "read")

        self.mock_output = self.patcher_output.start()
        self.mock_is_valid = self.patcher_is_valid.start()
        self.mock_read = self.patcher_read.start()

    def tearDown(self):
        self.patcher_output.stop()
        self.patcher_is_valid.stop()
        self.patcher_read.stop()

    def test_read_validValue(self):
        expected_value = "expected"
        self.mock_is_valid.side_effect = [False, True]
        self.mock_read.return_value = expected_value

        result = self.reader.read()

        self.assertEqual(result, expected_value)
        self.mock_is_valid.assert_has_calls([call(None), call(expected_value)])
        self.mock_output.assert_called_once()

    def test_read_invalidValue_thenValid(self):
        bad_value = "bad value"
        good_value = "expected"

        self.mock_is_valid.side_effect = [False, False, True]
        self.mock_read.side_effect = [bad_value, good_value]

        result = self.reader.read()

        self.assertEqual(result, good_value)
        self.mock_is_valid.assert_has_calls([call(None), call(bad_value), call(good_value)])


if __name__ == '__main__':
    unittest.main()
