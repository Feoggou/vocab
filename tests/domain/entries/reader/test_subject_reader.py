import unittest

from vocabulary.domain.entries.reader.subject import SubjectReader
from vocabulary.app.tag.subject_catalog import SubjectCatalog


class SubjectReaderTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        reader = SubjectReader(msg="dummy")
        self.assertIsInstance(reader.get_catalog(), SubjectCatalog)


if __name__ == '__main__':
    unittest.main()
