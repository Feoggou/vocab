import unittest
from unittest.mock import patch

from vocabulary.domain.entries.reader.line_list import LineListReader
from vocabulary.domain.entries.reader.item import ItemReader


class LineListReaderTest(unittest.TestCase):
    def setUp(self):
        self.reader = LineListReader("dummy")
        self.patcher_read = patch.object(ItemReader, "read")

        self.mock_read = self.patcher_read.start()

    def tearDown(self):
        self.patcher_read.stop()

    def test_read_oneItem(self):
        self.mock_read.return_value = "item"

        result = self.reader.read()

        self.assertListEqual(result, ["item"])

    def test_read_twoItems(self):
        self.mock_read.return_value = "item1, item2"

        result = self.reader.read()

        self.assertListEqual(result, ["item1", "item2"])

    def test_read_noItem(self):
        self.mock_read.return_value = None

        result = self.reader.read()

        self.assertListEqual(result, [])


if __name__ == '__main__':
    unittest.main()
