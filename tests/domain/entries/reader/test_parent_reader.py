import unittest
from vocabulary.domain.entries.reader.parent import ParentReader

from vocabulary.app.tag.parent_catalog import ParentCatalog


class ParentReaderTest(unittest.TestCase):
    def test_createsCorrectCatalog(self):
        reader = ParentReader(msg="dummy")
        self.assertIsInstance(reader.get_catalog(), ParentCatalog)


if __name__ == '__main__':
    unittest.main()
