import unittest
from unittest.mock import patch

from vocabulary.domain.entries.derived_forms import DerivedForms, DerivedFormsReader
from vocabulary.domain.entries.reader.derived_forms_item_reader import DerivedFormsItemReader


class DerivedFormsTest(unittest.TestCase):
    @patch.object(DerivedFormsItemReader, "read")
    def test_can_read_derived_forms(self, derived_forms_read):
        expected_entry = DerivedForms(noun="1-noun", adj="2-adj", verb="3-verb", adv="4-adv", others="5-oth")
        derived_forms_read.side_effect = ["1-noun", "2-adj", "3-verb", "4-adv", "5-oth"]

        entry = DerivedFormsReader.read()

        self.assertEqual(entry, expected_entry)


if __name__ == '__main__':
    unittest.main()
