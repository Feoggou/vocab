import unittest
from unittest import mock

from vocabulary.cli.parser.cmd_builder import CommandBuilder
from vocabulary.cli.commands.command import Command
from vocabulary.app.application import Application, create_displayer


class ApplicationTest(unittest.TestCase):
    def test_givenTextAsInput_interpretsAndExecutesCommand(self):
        mock_command = self.mockCommand()
        mock_parser = self.mockParserReturningCommand(mock_command)

        app = Application(mock_parser, self.mockDisplayerCreator())
        app.process("<dummy read input>")

        mock_parser.build_from_line.assert_called_once_with("<dummy read input>")
        mock_command.execute.assert_called_once_with()

    def test_whenCommandYieldsResult_willDisplayIt(self):
        mock_displayer = mock.Mock()
        mock_command = self.mockCommand(command_result="a command result")
        mock_parser = self.mockParserReturningCommand(mock_command)
        mock_displayer_creator = self.mockDisplayerCreator(returned_displayer=mock_displayer)

        app = Application(mock_parser, mock_displayer_creator)
        app.process("a read input")

        mock_displayer_creator.assert_called_once_with("a command result")
        mock_displayer.execute.assert_called_once()

    def test_whenCommandYieldsNoResult_doesNotDisplayIt(self):
        mock_displayer = mock.Mock()
        mock_command = self.mockCommand(command_result=None)
        mock_parser = self.mockParserReturningCommand(returned_command=mock_command)
        mock_displayer_creator = self.mockDisplayerCreator(returned_displayer=mock_displayer)

        app = Application(mock_parser, mock_displayer_creator)
        app.process("<dummy read input>")

        mock_displayer.execute.assert_not_called()

    @staticmethod
    def mockDisplayerCreator(returned_displayer=mock.Mock()):
        mock_displayer_creator = mock.create_autospec(spec=create_displayer)
        mock_displayer_creator.return_value = returned_displayer

        return mock_displayer_creator

    @staticmethod
    def mockCommand(command_result: str = None):
        mock_command = mock.create_autospec(spec=Command)
        mock_command.execute.return_value = command_result

        return mock_command

    @staticmethod
    def mockParserReturningCommand(returned_command):
        mock_parser = mock.create_autospec(spec=CommandBuilder)
        mock_parser.build_from_line.return_value = returned_command

        return mock_parser


if __name__ == '__main__':
    unittest.main()
