import unittest


from vocabulary.cli.parser.cmd_parser import CommandParser
from vocabulary.utils.errors import *


class CommandParserTest(unittest.TestCase):
    def test_whenNoInputIsGiven_returnsNone(self):
        parser = CommandParser()

        command = parser.parse("")

        self.assertIsNone(command)

    def test_canExtractAttributesOfCommand_withNoOption(self):
        parser = CommandParser()

        command = parser.parse("search")

        self.assertEqual(command["name"], "search")
        self.assertEqual(command["options"], {})
        self.assertEqual(command["target"], "")

    def test_canExtractAttributesOfCommand_withOneOption(self):
        parser = CommandParser()

        command = parser.parse("search gram=vb")

        self.assertEqual(command["name"], "search")
        self.assertEqual(command["options"], {"gram": "vb"})

    def test_canExtractAttributesOfComplexCommand_withOneOption(self):
        parser = CommandParser()

        command = parser.parse("search with subcommand gram=vb gram2=x")

        self.assertEqual(command["name"], "search with subcommand")
        self.assertEqual(command["options"], {"gram": "vb", "gram2": "x"})

    def test_canExtractAttributesOfCommand_withTwoOptions(self):
        parser = CommandParser()

        command = parser.parse("search gram=vb domain=mydomain")

        self.assertEqual(command["name"], "search")
        self.assertEqual(command["options"], {"gram": "vb", "domain": "mydomain"})

    def test_canExtractAttributesOfCommand_withThreeOptions(self):
        parser = CommandParser()

        command = parser.parse("search gram=vb domain=mydomain parent=myparent")

        self.assertEqual(command["name"], "search")
        self.assertEqual(command["options"], {"gram": "vb", "domain": "mydomain", "parent": "myparent"})

    def test_whenExtractingAttributesOfCommand_withOptionWithoutValue_raisesError(self):
        parser = CommandParser()

        with self.assertRaisesRegex(BadlyFormattedCommand, 'gram'):
            parser.parse("search gram=")

    def test_whenExtractingAttributesOfCommand_withOptionWithoutKey_raisesError(self):
        parser = CommandParser()

        with self.assertRaisesRegex(BadlyFormattedCommand, 'vb'):
            parser.parse("search =vb")

    def test_canExtractFullCommandName_whenCommandHasMultipleWords(self):
        parser = CommandParser()

        command = parser.parse("add def")

        self.assertEqual(command["name"], "add def")
        self.assertEqual(command["options"], {})

    def test_whenCommandNameIsMissing_raisesError(self):
        parser = CommandParser()

        with self.assertRaises(BadlyFormattedCommand):
            parser.parse("option=value")

    def test_canExtractOptionValue_whenItsQuoted(self):
        parser = CommandParser()

        command = parser.parse("search gram=\"vb\"")

        self.assertEqual(command["name"], "search")
        self.assertEqual(command["options"], {"gram": "vb"})

    def test_canExtractOptionValueInMultiword_whenItsQuoted(self):
        parser = CommandParser()

        command = parser.parse("search domain=\"my domain\"")

        self.assertEqual(command["name"], "search")
        self.assertEqual(command["options"], {"domain": "my domain"})

    def test_whenExtractionOptionValueInMultiwordQuoted_whitespacesArePreserved(self):
        parser = CommandParser()

        command = parser.parse("search domain=\"my   domain\"")

        self.assertEqual(command["name"], "search")
        self.assertEqual(command["options"], {"domain": "my   domain"})

    def test_canParseCommand_withColon(self):
        parser = CommandParser()

        command = parser.parse("quit:")

        self.assertEqual(command["name"], "quit")
        self.assertEqual(command["target"], "")

    def test_optionsAndTargetAreEmpty_ifNotGiven(self):
        parser = CommandParser()

        command = parser.parse("add:")

        self.assertEqual(command["options"], {})
        self.assertEqual(command["target"], "")

    def test_ifColonIsMissing_thenTargetIsEmpty(self):
        parser = CommandParser()

        command = parser.parse("add")

        self.assertEqual(command["target"], "")

    def test_canParseCommand_withTarget(self):
        parser = CommandParser()

        command = parser.parse("add: target")

        self.assertEqual(command["name"], "add")
        self.assertEqual(command["target"], "target")

    def test_canParseCommandWithTarget_whenThereIsSpaceBeforeColon(self):
        parser = CommandParser()

        command = parser.parse("add : target")

        self.assertEqual(command["name"], "add")
        self.assertEqual(command["target"], "target")

    def test_canParsecommandWithTargetAndOneOption(self):
        parser = CommandParser()

        command = parser.parse("search gram=vb: a target")

        self.assertEqual(command["name"], "search")
        self.assertEqual(command["options"], {"gram": "vb"})
        self.assertEqual(command["target"], "a target")

    def test_canParseCommandWithEmptySubcommandName(self):
        parser = CommandParser()

        command = parser.parse('add "" key=value')

        self.assertEqual(command["name"], "add")
        self.assertEqual(command["options"], {"key": "value"})

    def test_whenTargetHasQuotes_allQuotesAreRemoved(self):
        parser = CommandParser()

        command = parser.parse('add: I think "def: target:" should be')

        self.assertEqual(command["name"], "add")
        self.assertEqual(command["target"], 'I think def: target: should be')

    def test_canParse_whenAdditionalColonResidesInQuotesAsValue(self):
        parser = CommandParser()

        command = parser.parse("add def an_option=\"colon: a colon here\" option2=3: a target")

        self.assertEqual(command["name"], "add def")
        self.assertEqual(command["options"], {"an_option": "colon: a colon here", "option2": "3"})
        self.assertEqual(command["target"], "a target")


if __name__ == "__main__":
    unittest.main()
