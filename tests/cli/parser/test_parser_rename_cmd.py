import unittest

from vocabulary.cli.commands.rename_parser import RenameCommandParser
from vocabulary.utils.errors import *


class RenameCommandParserTest(unittest.TestCase):
    def setUp(self):
        self.parser = RenameCommandParser()

    def test_parse_simple(self):
        old, new = self.parser.parse_expression("old_name -> new_name")

        self.assertEqual(old, "old_name")
        self.assertEqual(new, "new_name")

    def test_parse_withNoSpaces(self):
        old, new = self.parser.parse_expression("old_name->new_name")

        self.assertEqual(old, "old_name")
        self.assertEqual(new, "new_name")

    def test_parse_withQuotesAndSpaces(self):
        old, new = self.parser.parse_expression('"old name" -> "new name"')

        self.assertEqual(old, "old name")
        self.assertEqual(new, "new name")

    def test_parse_badlyFormat_noArrow(self):
        with self.assertRaises(BadlyFormattedCommand):
            self.parser.parse_expression("no arrow here")

    def test_parse_badlyFormat_tooManyArrows(self):
        with self.assertRaises(BadlyFormattedCommand):
            self.parser.parse_expression("a -> b -> c")


if __name__ == '__main__':
    unittest.main()
