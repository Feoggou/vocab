import unittest

from vocabulary.cli.parser.cmd_finder import CommandFinder
from vocabulary.utils.errors import *
from vocabulary.cli.commands import *


class CommandFinderTest(unittest.TestCase):
    def test_ifCannotFindCommand_raisesError(self):
        finder = CommandFinder()

        with self.assertRaises(CommandNotFoundError) as e:
            finder.find_class(command_name="a-nonexisting-command")

        self.assertEqual(e.exception.cmd_name, "a-nonexisting-command")

    def test_canFindCommand_quit(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="quit")

        self.assertEqual(command, QuitCommand)

    def test_canFindCommand_exit_as_quit(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="exit")

        self.assertEqual(command, QuitCommand)

    def test_canFindCommand_help(self):
        finder = CommandFinder()

        command = finder.find_class("help")

        self.assertEqual(command, HelpCommand)

    def test_canFindCommand_display(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="display")

        self.assertEqual(command, DisplayCommand)

    def test_canFindCommand_print_as_display(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="print")

        self.assertEqual(command, DisplayCommand)

    def test_canFindCommand_p_as_display(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="p")

        self.assertEqual(command, DisplayCommand)

    def test_canFindCommand_addDefinition(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="add def")

        self.assertEqual(command, AddDefinitionCommand)

    def test_canFindCommand_newBare(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="add")

        self.assertEqual(command, NewBareCommand)

    def test_canFindCommand_addIdiom(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="add idiom")

        self.assertEqual(command, AddIdiomCommand)

    def test_canFindCommand_addDomain(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="add domain")

        self.assertEqual(command, AddDomainCommand)

    def test_canFindCommand_addStyle(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="add style")

        self.assertEqual(command, AddStyleCommand)

    def test_canFindCommand_addObject(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="add object")

        self.assertEqual(command, AddObjectCommand)

    def test_canFindCommand_addParent(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="add parent")

        self.assertEqual(command, AddParentCommand)

    def test_canFindCommand_addSubject(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="add subject")

        self.assertEqual(command, AddSubjectCommand)

    def test_canFindCommand_showDomains(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="show domains")

        self.assertEqual(command, ShowDomainsCommand)

    def test_canFindCommand_showStyles(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="show styles")

        self.assertEqual(command, ShowStylesCommand)

    def test_canFindCommand_showObjects(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="show objects")

        self.assertEqual(command, ShowObjectsCommand)

    def test_canFindCommand_showParents(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="show parents")

        self.assertEqual(command, ShowParentCommand)

    def test_canFindCommand_showSubjects(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="show subjects")

        self.assertEqual(command, ShowSubjectsCommand)

    def test_canFindCommand_search(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="search")

        self.assertEqual(command, SearchExplanationCommand)

    def test_canFindCommand_usage(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="usage")

        self.assertEqual(command, UsageCommand)

    def test_canFindCommand_list(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="list")

        self.assertEqual(command, ListCommand)

    def test_canFindCommand_vim(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="vim")

        self.assertEqual(command, VimCommand)

    def test_canFindCommand_renameParent(self):
        finder = CommandFinder()

        command = finder.find_class(command_name="rename parent")

        self.assertEqual(command, RenameParentCommand)


if __name__ == "__main__":
    unittest.main()
