import unittest
from unittest.mock import patch, Mock

from vocabulary.cli.commands.rename_parent import RenameParentCommand

from vocabulary.app.tag.parent_catalog import ParentCatalog

from vocabulary.cli.commands.rename_parser import RenameCommandParser


class RenameParentCommandTest(unittest.TestCase):
    def test_execute_renamesParentsFile_and_renamesFolderItems(self):
        repo = Mock()
        cmd = RenameParentCommand(repo, "old_parent -> new_parent")

        with patch.object(RenameCommandParser, "parse_expression") as mock_parse:
            mock_parse.return_value = ["old_parent", "new_parent"]
            with patch.object(ParentCatalog, "rename_item") as mock_rename_parent:
                cmd.execute()

        mock_parse.assert_called_once_with("old_parent -> new_parent")
        mock_rename_parent.assert_called_once_with("old_parent", "new_parent")
        repo.rename_tag.assert_called_once_with("parents", "old_parent", "new_parent")


if __name__ == "__main__":
    unittest.main()
