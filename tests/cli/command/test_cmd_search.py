import unittest
from unittest.mock import patch

from vocabulary.cli.commands.search import SearchCommand

from vocabulary.app.search.filter.gram_value import GramValueFilter
from vocabulary.app.search.method_strategy import SearchMethodStrategy
from vocabulary.app.search.where import SearchNowhere


class SearchCommandTest(unittest.TestCase):
    def test_noOptions_creates_noFilters(self):
        with patch.object(SearchMethodStrategy, "pick_method_for"):
            cmd_search = SearchCommand("<dummy>", opts={}, search_in=SearchNowhere())

        filters = cmd_search.get_filters()

        self.assertEqual(len(filters), 0)

    def test_whenHaveOptions_calls_createFilters(self):
        expected_filters = ["filter1", "filter2", "filter3"]

        with patch.object(SearchCommand, "create_filters") as mock_create_filters:
            mock_create_filters.return_value = expected_filters
            with patch.object(SearchMethodStrategy, "pick_method_for"):
                cmd_search = SearchCommand("<dummyword> gram=vb domain=mydomain", opts={}, search_in=SearchNowhere())

        mock_create_filters.assert_called_once()
        self.assertEqual(cmd_search.get_filters(), expected_filters)

    def test_createFilter(self):
        expected_filter = GramValueFilter("vb")

        filters = SearchCommand.create_filters({"gram": "vb"})

        self.assertEqual(len(filters), 1)
        self.assertEqual(filters[0], expected_filter)


if __name__ == "__main__":
    unittest.main()
