from vocabulary.domain.entries.definition import Definition
from vocabulary.domain.entries.idiom import Idiom


def a_definition(gram_value="", explanations=list(), notes="", usefulness=-1, examples=list(), domains=list(),
                 subjects=list(), objects=list(), parents=list(), styles=list()):
    return Definition(gram_value=gram_value, explanations=explanations, notes=notes, usefulness=usefulness,
                      examples=examples, domains=domains, subjects=subjects, objects=objects, parents=parents,
                      styles=styles)


def an_idiom(expression: str, **kwargs):
    definition = a_definition(**kwargs)
    return Idiom.from_definition(expression=expression, definition=definition)
